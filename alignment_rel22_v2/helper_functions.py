import ROOT

def tower_index(event):
    index1 = abs(event.seg1_eta_index)
    if event.seg1_name_short == "cs":
        index1 = 0
    elif event.seg1_name_short == "ei" and event.seg1_cham_index == 8:
        if event.comb_sector in [1,9]:
            if event.seg1_eta_index == 4:
                index1 = 5
            elif event.seg1_eta_index == 5:
                index1 = 4   
        
    return "{}{}{}".format(index1,abs(event.seg2_eta_index),abs(event.seg3_eta_index))


def sider(event):
    sside = None
    if event.comb_side%2 == 0:
        sside = 'a'
    elif event.comb_side%2 == 1:
        sside = 'c'
    return sside


def muon_pt(event):
    pt = int(event.segger1_muon_pt*ROOT.TMath.CosH(event.segger1_muon_eta)/1000.)
    if pt < 100:
        return "0"+str(pt)
    if pt > 999:
        return "999"
    else:
        return str(pt)


def local(event):
    x1 = -event.segger1_hitavg_local_x
    x2 = -event.segger2_hitavg_local_x
    x3 = -event.segger3_hitavg_local_x
    y1 = event.seg1_local_y
    z1 = event.seg1_local_z
    y2 = event.seg2_local_y
    z2 = event.seg2_local_z
    y3 = event.seg3_local_y
    z3 = event.seg3_local_z
    if event.seg1_name_short == "cs":
        if "L" in event.seg1_name:
            y1 = y1+ 1129.2/2.-17.7
            z1 = z1 + 175.7+123.5/2.-38.51;
        elif "S" in event.seg1_name:
            y1 = y1 + 1176.9/2.-17.7
            z1 = z1 + 175.7+123.5/2.-38.51
    if comber_type(event) == 3 and sider(event) == "a":
        x2 = -x2
    seg1 = "{:.3f} {:.3f} {:.3f} {:.6g}".format(x1,z1,y1,-ROOT.TMath.ATan(event.seg1_local_py/event.seg1_local_pz))
    seg2 = "{:.3f} {:.3f} {:.3f} {:.6g}".format(x2,z2,y2,-ROOT.TMath.ATan(event.seg2_local_py/event.seg2_local_pz))
    seg3 = "{:.3f} {:.3f} {:.3f} {:.6g}".format(x3,z3,y3,-ROOT.TMath.ATan(event.seg3_local_py/event.seg3_local_pz))
    seg4 = "{:.6g} {:.6g} {:.6g}".format(event.vx_x, event.vx_y, event.vx_z)
    return seg1,seg2,seg3,seg4
    
    
def comber_type(event):
    s1 = str(event.seg1_name_short)
    s2 = str(event.seg2_name_short)
    s3 = str(event.seg3_name_short)
    name = s1+s2+s3
    if name == "eiemeo":
        return 1
    elif name == "eieeem":
        return 2
    elif name == "eibeem":
        return 3
    elif name == "csemeo":
        return 4
    else:
        print("NOOOOOPE")
        print(name,"is not a known combination")


def namer(event, seg):
    if seg == 1:
        if event.comb_sector < 10:
            return "{}_r{}_e{}_t{}_1_{}{}0{}_{}".format(event.comb_type,
                                                        event.cRunNumber,
                                                        event.cEventNumber,
                                                        tower_index(event),
                                                        sider(event),
                                                        event.seg1_name_short,
                                                        event.comb_sector,
                                                        muon_pt(event))
        else:
            return "{}_r{}_e{}_t{}_1_{}{}{}_{}".format(event.comb_type,
                                                       event.cRunNumber,
                                                       event.cEventNumber,
                                                       tower_index(event),
                                                       sider(event),
                                                       event.seg1_name_short,
                                                       event.comb_sector,
                                                       muon_pt(event))
    elif seg == 2:
        if event.comb_sector < 10:
            return "{}_r{}_e{}_t{}_1_{}{}0{}_{}".format(event.comb_type,
                                                        event.cRunNumber,
                                                        event.cEventNumber,
                                                        tower_index(event),
                                                        sider(event),
                                                        event.seg2_name_short,
                                                        event.comb_sector,
                                                        muon_pt(event))
        else:
            return "{}_r{}_e{}_t{}_1_{}{}{}_{}".format(event.comb_type,
                                                       event.cRunNumber,
                                                       event.cEventNumber,
                                                       tower_index(event),
                                                       sider(event),
                                                       event.seg2_name_short,
                                                       event.comb_sector,
                                                       muon_pt(event))
    elif seg == 3:
        if event.comb_sector < 10:
            return "{}_r{}_e{}_t{}_1_{}{}0{}_{}".format(event.comb_type,
                                                        event.cRunNumber,
                                                        event.cEventNumber,
                                                        tower_index(event),
                                                        sider(event),
                                                        event.seg3_name_short,
                                                        event.comb_sector,
                                                        muon_pt(event))
        else:
            return "{}_r{}_e{}_t{}_1_{}{}{}_{}".format(event.comb_type,
                                                       event.cRunNumber,
                                                       event.cEventNumber,
                                                       tower_index(event),
                                                       sider(event),
                                                       event.seg3_name_short,
                                                       event.comb_sector,
                                                       muon_pt(event))
    elif seg == 4:
        if event.comb_sector < 10:
            return "{}_r{}_e{}_t{}_1_{}{}0{}_{}".format(event.comb_type,
                                                        event.cRunNumber,
                                                        event.cEventNumber,
                                                        tower_index(event),
                                                        sider(event),
                                                        "ip",
                                                        event.comb_sector,
                                                        muon_pt(event))
        else:
            return "{}_r{}_e{}_t{}_1_{}{}{}_{}".format(event.comb_type,
                                                       event.cRunNumber,
                                                       event.cEventNumber,
                                                       tower_index(event),
                                                       sider(event),
                                                       "ip",
                                                       event.comb_sector,
                                                       muon_pt(event))

        

def origin(event, seg):
    org = sider(event)
    if seg == 1:
        if event.seg1_name_short == "cs":
            segname = "ei"
            segindex = "c"
        else:
            segname = event.seg1_name_short
            segindex = str(abs(event.seg1_eta_index))
    elif seg == 2:
        segname = event.seg2_name_short
        segindex = str(abs(event.seg2_eta_index))
    elif seg ==3:
        segname = event.seg3_name_short
        segindex = str(abs(event.seg3_eta_index))
        
    theorg = "_org"
    
    if seg == 1 and event.seg1_name_short == "ei" and event.seg1_cham_index == 8:
        if event.comb_sector in [7,11,15] and abs(event.seg1_eta_index)==4:
            theorg = "_sorg"
        elif event.comb_sector in [11,15] and abs(event.seg1_eta_index)==3:
            theorg = "_sorg"
        elif event.comb_sector in [1,9] and abs(event.seg1_eta_index)==4:
            segindex = 5
        elif event.comb_sector in [1,9] and abs(event.seg1_eta_index)==5:
            segindex = 4
    
            
    org += str(segname)+"_0"
    org += str(event.comb_sector)+"_c"
    org += str(segindex) + theorg

    
    return org


def seg_bounder(event):
    useEIL_index = abs(event.seg1_eta_index)
    if event.comb_sector in [1,9] and event.seg1_cham_index == 8:
        if abs(event.seg1_eta_index) == 4:
            useEIL_index = 5
        elif abs(event.seg1_eta_index) == 5:
            useEIL_index = 4

    # dimensions for chambers        
    EIL_frame =[6*6*30.035,
                6*6*30.035,
                2*6*30.035,
                9*6*30.035,
                1*6*30.035]
    EIS_frame = [7*6*30.035,
                 6*6*30.035]
    EML_frame = [7*8*30.035,
                8*8*30.035,
                8*8*30.035,
                8*8*30.035,
                8*8*30.035]
    EMS_frame = [8*8*30.035,
                 8*8*30.035,
                 8*8*30.035,
                 8*8*30.035,
                 8*8*30.035]
    EOL_frame = [7*8*30.035,
                 7*8*30.035,
                 6*8*30.035,
                 6*8*30.035,
                 6*8*30.035,
                 6*8*30.035]
    EOS_frame = [7*8*30.035,
                 7*8*30.035,
                 7*8*30.035,
                 6*8*30.035,
                 6*8*30.035,
                 6*8*30.035]
    EEL_frame = [5*8*30.035,
                 5*8*30.035]
    EES_frame = [6*8*30.035,
                 5*8*30.035]
    BEE_frame = [6*8*30.035,
                 6*8*30.035]

    seg1_bound = False
    seg2_bound = False
    seg3_bound = False
    
    comb_id = comber_type(event)
    
    if comb_id == 1:
        if (event.seg1_cham_index == 7 and 
            event.seg1_local_z < (EIS_frame[abs(event.seg1_eta_index) - 1] - 45.) and
            event.seg1_local_z > 45.):
            seg1_bound = True
        elif (event.seg1_cham_index == 8 and 
            event.seg1_local_z < (EIL_frame[useEIL_index - 1] - 45.) and
            event.seg1_local_z > 45.):
            seg1_bound = True
        if (event.seg2_cham_index == 9 and
            event.seg2_local_z < (EMS_frame[abs(event.seg2_eta_index) - 1] - 45.) and
            event.seg2_local_z > 45.):
            seg2_bound = True
        elif (event.seg2_cham_index == 10 and 
            event.seg2_local_z < (EML_frame[abs(event.seg2_eta_index) - 1] - 45.) and
            event.seg2_local_z > 45.):
            seg2_bound = True
        if (event.seg3_cham_index == 11 and
            event.seg3_local_z < (EOS_frame[abs(event.seg3_eta_index) - 1] - 45.) and
            event.seg3_local_z > 45.):
            seg3_bound = True
        elif (event.seg3_cham_index == 12 and 
            event.seg3_local_z < (EOL_frame[abs(event.seg3_eta_index) - 1] - 45.) and
            event.seg3_local_z > 45.):
            seg3_bound = True
    elif comb_id == 2:
        if (event.seg1_cham_index == 7 and 
            event.seg1_local_z < (EIS_frame[abs(event.seg1_eta_index) - 1] - 45.) and
            event.seg1_local_z > 45.):
            seg1_bound = True
        elif (event.seg1_cham_index == 8 and 
            event.seg1_local_z < (EIL_frame[useEIL_index - 1] - 45.) and
            event.seg1_local_z > 45.):
            seg1_bound = True
        if (event.seg2_cham_index == 13 and
            event.seg2_local_z < (EES_frame[abs(event.seg2_eta_index) - 1] - 45.) and
            event.seg2_local_z > 45.):
            seg2_bound = True
        elif (event.seg2_cham_index == 14 and 
            event.seg2_local_z < (EEL_frame[abs(event.seg2_eta_index) - 1] - 45.) and
            event.seg2_local_z > 45.):
            seg2_bound = True
        if (event.seg3_cham_index == 9 and
            event.seg3_local_z < (EMS_frame[abs(event.seg3_eta_index) - 1] - 45.) and
            event.seg3_local_z > 45.):
            seg3_bound = True
        elif (event.seg3_cham_index == 10 and 
            event.seg3_local_z < (EML_frame[abs(event.seg3_eta_index) - 1] - 45.) and
            event.seg3_local_z > 45.):
            seg3_bound = True
    elif comb_id == 3:
        if (event.seg1_cham_index == 7 and 
            event.seg1_local_z < (EIS_frame[abs(event.seg1_eta_index) - 1] - 45.) and
            event.seg1_local_z > 45.):
            seg1_bound = True
        elif (event.seg1_cham_index == 8 and 
            event.seg1_local_z < (EIL_frame[useEIL_index - 1] - 45.) and
            event.seg1_local_z > 45.):
            seg1_bound = True
        if (event.seg2_cham_index == 6 and
            event.seg2_local_z < (BEE_frame[abs(event.seg2_eta_index) - 1] - 45.) and
            event.seg2_local_z > 45.):
            seg2_bound = True
        if (event.seg3_cham_index == 9 and
            event.seg3_local_z < (EMS_frame[abs(event.seg3_eta_index) - 1] - 45.) and
            event.seg3_local_z > 45.):
            seg3_bound = True
        elif (event.seg3_cham_index == 10 and 
            event.seg3_local_z < (EML_frame[abs(event.seg3_eta_index) - 1] - 45.) and
            event.seg3_local_z > 45.):
            seg3_bound = True
    if comb_id == 4:
        if event.seg1_cham_index == 15:
            seg1_bound = True
        elif event.seg1_cham_index == 16:
            seg1_bound = True
        if (event.seg2_cham_index == 9 and
            event.seg2_local_z < (EMS_frame[abs(event.seg2_eta_index) - 1] - 45.) and
            event.seg2_local_z > 45.):
            seg2_bound = True
        elif (event.seg2_cham_index == 10 and 
            event.seg2_local_z < (EML_frame[abs(event.seg2_eta_index) - 1] - 45.) and
            event.seg2_local_z > 45.):
            seg2_bound = True
        if (event.seg3_cham_index == 11 and
            event.seg3_local_z < (EOS_frame[abs(event.seg3_eta_index) - 1] - 45.) and
            event.seg3_local_z > 45.):
            seg3_bound = True
        if (event.seg3_cham_index == 12 and 
            event.seg3_local_z < (EOL_frame[abs(event.seg3_eta_index) - 1] - 45.) and
            event.seg3_local_z > 45.):
            seg3_bound = True
    
    return seg1_bound and seg2_bound and seg3_bound


def precision_cutter(event):
    comb_id = comber_type(event)
    passed = False
    if comb_id in [1,3]:
        if event.seg1_hitreq and event.seg2_hitreq and event.seg3_hitreq:
            passed = True
    elif comb_id == 2:
        if event.seg1_hitreq and event.seg2_nPrecisionHits > 5 and event.seg3_hitreq:
            passed = True
            
    elif comb_id == 4:
        if event.seg1_nPrecisionHits > 2 and event.seg2_hitreq and event.seg3_hitreq:
            passed = True
            
    return passed


def pass_cuts(event):
    passed = {'no cuts':True}
    
    # fit quality cut
    passed['fit quality'] = (event.seg1_chisquare/event.seg1_nDOF < 10. and
               event.seg2_chisquare/event.seg2_nDOF < 10. and
               event.seg3_chisquare/event.seg3_nDOF < 10.)
    
    # N precision hits
    passed['n precision hits'] = precision_cutter(event)

    # TGC phi hit
    passed['TGC phi hit'] = ((comber_type(event) in [1,4] and event.seg2_nTrigPhiLayers > 0) or
                             (comber_type(event) in [2,3] and event.seg3_nTrigPhiLayers > 0))

    # track hit pointing
    passed["track hit pointing"] = pt_checker(event)
    
    # track hit sharing
    passed['track hit sharing'] = ((event.segger1_track_hitshare >= 4 and
                                        event.segger1_track_hitshare >= event.segger1_nhits - 1) and
                                       (event.segger2_track_hitshare >= 4 and
                                        event.segger2_track_hitshare >= event.segger2_nhits - 1) and
                                       (event.segger3_track_hitshare >= 4 and
                                        event.segger3_track_hitshare >= event.segger3_nhits - 1))

    # Segment pointing
    passed["segment pointing"] = pointing(event)
    
    # Segment hit sharing
    passed["segment hit sharing"] = sharing(event)
    
    # Muon author
    passed["muon author"] = (event.segger1_muon_author == event.segger2_muon_author and
                            event.segger1_muon_author == event.segger3_muon_author and
                            event.segger1_muon_author != -999999)
    # High pt / forward
    passed['high pt'] = (passed['track hit pointing'] and 
                         (event.segger1_muon_pt*ROOT.TMath.CosH(event.segger1_muon_eta) > 40000. or
                          event.segger1_muon_pt > 10000.))
    passed['Seg bound'] = seg_bounder(event)
    
    return passed


# this always passes
def sharing(event):
    s1_count = 0
    s2_count = 0
    s3_count = 0
    passed = True
    
    for track in event.segger1_hitshare:
        print('track 1', event.segger1_hitshare)
        print('with track',track)
        if track > 0:
            passed = False
            s1_count+=1
    for track in event.segger2_hitshare:
        print('track 2', event.segger2_hitshare)
        print('with track',track)
        if track > 0:
            passed = False
            s2_count+=1
    for track in event.segger3_hitshare:
        if track > 0:
            passed = False
            s3_count+=1
           
    if not passed and s1_count == 1 and s2_count == 1 and s3_count == 1:
        print('countess')
        passed = True
        
    if not passed:
        print("failure!")
    return passed


def pt_checker(event):
    passed = (event.segger1_muon_pt != -999999 and
              event.segger2_muon_pt != -999999 and
              event.segger3_muon_pt != -999999)
    same2 = (event.segger1_muon_pt == event.segger2_muon_pt and
             event.segger1_muon_eta == event.segger2_muon_eta and
             event.segger1_muon_phi == event.segger2_muon_phi)
    same3 = (event.segger1_muon_pt == event.segger3_muon_pt and
             event.segger1_muon_eta == event.segger3_muon_eta and
             event.segger1_muon_phi == event.segger3_muon_phi)
    return passed and same2 and same3


def pointing(event):
    # first define variables
    Seg1 = ROOT.TVector3(event.seg1_global_x, event.seg1_global_y, event.seg1_global_z)
    Seg2 = ROOT.TVector3(event.seg2_global_x, event.seg2_global_y, event.seg2_global_z)
    Seg3 = ROOT.TVector3(event.seg3_global_x, event.seg3_global_y, event.seg3_global_z)
    
    seg1_seg2_deta = Seg1.Eta() - Seg2.Eta()
    seg1_seg3_deta = Seg1.Eta() - Seg3.Eta()
    seg2_seg3_deta = Seg2.Eta() - Seg3.Eta()
    
    seg1_seg2_dphi = Seg1.DeltaPhi(Seg2)
    seg1_seg3_dphi = Seg1.DeltaPhi(Seg3)
    seg2_seg3_dphi = Seg2.DeltaPhi(Seg2)
    
    passed = False
    if event.seg1_name_short == "cs":
        if abs(seg1_seg2_deta) < 0.3 and abs(seg1_seg2_dphi) < 3:
            if abs(seg1_seg3_deta) < 0.3  and abs(seg1_seg3_dphi) < 3:
                if abs(seg2_seg3_deta) < 0.05 and abs(seg2_seg3_dphi) < 0.5:
                    passed = True
    else:
        if abs(seg1_seg2_deta) < 0.05 and abs(seg1_seg2_dphi) < 0.5:
            if abs(seg1_seg3_deta) < 0.05  and abs(seg1_seg3_dphi) < 0.5:
                if abs(seg2_seg3_deta) < 0.05 and abs(seg2_seg3_dphi) < 0.5:
                    passed = True        
    return passed

