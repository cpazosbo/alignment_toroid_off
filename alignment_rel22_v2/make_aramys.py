#!/usr/bin/env python

import sys
import ROOT
import numpy as np
from array import array
from helper_functions import *

assert len(sys.argv)>1, "need at least one input file"
print("running on file", sys.argv[-1])
infile = ROOT.TFile.Open(sys.argv[-1])
tree = infile.Get('tree')


n_passed = np.zeros(11)
n_cum = np.zeros(11)
n = 0
n_tot = 0

with open("out.dat", 'a') as f:
    sys.stdout = f

    for event in tree:
        n_tot += 1
        cuts = pass_cuts(event).values()
        n_passed += np.array(list(cuts))
        n_cum += np.array([all(list(cuts)[:i]) for i in range(1,len(cuts)+1)])
        
        if all(cuts):
            n += 1
            seg1, seg2, seg3, seg4 = local(event)
            print(namer(event,1),seg1, 0, 0, origin(event,1))
            print(namer(event,2),seg2, 0, 0, origin(event,2))
            print(namer(event,3),seg3, 0, 0, origin(event,3))
            print(namer(event,4),seg4,"0 0 0","tracks_org")

with open("summary.txt","a") as f:
    sys.stdout = f
    print("file",sys.argv[-1])
    print(n, "out of", n_tot)
    print("n passed",n_passed)
    print("n cumulative",n_cum)
