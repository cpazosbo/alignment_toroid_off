# Alignment Ntuple Anaysis Rel22

new version of the code 
```
git clone https://gitlab.cern.ch/cpazosbo/alignment_toroid_off.git
```

change into that directory
```
cd alignment_rel22
```

then set up the environment and compile. 
```
setupATLAS
source seganalysis_setup.sh
```

should compile. Then:

```
cd ../run
athena ../source/MyAnalysis/share/test_jobOptions.py
```

This *should* but doesn't yet produce a file called out.root that you run the *make_aramys.py* script on to produce the right input for aramys.
