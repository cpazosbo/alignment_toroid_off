testFileList = ["/afs/cern.ch/user/l/lucam/public/NSWQuadSegments.ESD.pool.root"]

### Basic job elements
from AthenaCommon.AppMgr import ServiceMgr       ## get a handle to the ServiceManager
from AthenaCommon.AlgSequence import AlgSequence ## get a handle to the default top-level algorithm sequence
topSequence = AlgSequence()

### Common Flags
from AthenaCommon.AthenaCommonFlags import jobproperties,athenaCommonFlags
jobproperties.AthenaCommonFlags.FilesInput = testFileList
jobproperties.AthenaCommonFlags.AccessMode = 'POOLAccess'
jobproperties.AthenaCommonFlags.EvtMax     = -1
jobproperties.AthenaCommonFlags.AllowIgnoreConfigError = True

### Global Flags 
#DetDescrVersion = "ATLAS-R3S-2021-02-00-00"
#jobproperties.Global.DataSource      = 'geant4'
#jobproperties.Global.InputFormat     = 'pool'
#jobproperties.Global.DetDescrVersion = DetDescrVersion
#jobproperties.Global.ConditionsTag   = 'OFLCOND-MC21-SDR-RUN3-06'

include ("RecExCommon/RecExCommon_topOptions.py")

from AthenaConfiguration.ComponentFactory import CompFactory
MuonDetectorCondAlg = CompFactory.MuonDetectorCondAlg
MuonDetectorCondAlg.applyMmPassivation=False

##########################################
### Add my algorithm
from MyAnalysis.MyAnalysisConf import MyAnalysisAlg
alg = MyAnalysisAlg('SteliosAnalysis')
topSequence += alg

##########################################
include("AthAnalysisBaseComps/SuppressLogging.py")
from AthenaCommon.AppMgr import ServiceMgr
ServiceMgr.MessageSvc.OutputLevel = WARNING
#ServiceMgr.MessageSvc.OutputLevel = VERBOSE

