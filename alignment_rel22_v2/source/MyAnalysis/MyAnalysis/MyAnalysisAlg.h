#ifndef MyAnalysis_MyAnalysis_H
#define MyAnalysis_MyAnalysis_H

#include "AthenaBaseComps/AthHistogramAlgorithm.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonSegment.h"
#include "TrkTrack/Track.h"
#include "TH1.h"
#include <vector>
#include "muonEvent/Muon.h"
#include "MuonSegment/MuonSegment.h"

#include "AthLinks/ElementLink.h"
#include "AthContainers/DataVector.h"
#include "AtlasDetDescr/AtlasDetectorID.h"
//#include "InDetReadoutGeometry/PixelDetectorManager.h"
#include "TrkExInterfaces/IExtrapolator.h"
#include "GaudiKernel/ToolHandle.h"


class MdtIdHelper;
class CscIdHelper;
class RpcIdHelper;
class TgcIdHelper;
//class MdtCalibrationSvc;
//namespace Muon {
class MuonEDMHelperTool;
//class MuonEDMPrinterTool;
//}
namespace MuonGM {
  class MuonDetectorManager;
}
class MyAnalysisAlg: public ::AthHistogramAlgorithm {
 public:
  MyAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~MyAnalysisAlg();

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

  virtual double DeltaR(double phi1, double eta1, double phi2, double eta2);
  virtual int trkMatchIndex(double tp_phi, double tp_eta, double &valdR);
  virtual std::string ChamberIndexToString (int i );
  virtual std::string hitChamberType (const xAOD::MuonSegment* muonseg);

  //  ToolHandle<Muon::MuonEDMPrinterTool>       m_printer;      //<! helper to nicely print out tracks
  //  ToolHandle<Muon::MuonEDMHelperTool>        m_helperTool; 

  StoreGateSvc* m_storeGateSvc;
  /** Pointer to MuonDetectorManager */
  const MuonGM::MuonDetectorManager*  m_detMgr;
  /** Pointers to the Identifier Helpers */
  const MdtIdHelper*  m_mdtIdHelper;
  const CscIdHelper*  m_cscIdHelper;
  const RpcIdHelper*  m_rpcIdHelper;
  const TgcIdHelper*  m_tgcIdHelper;


  //  const Trk::Extrapolator* m_extrapolator;
  //Not sure about this:
  //  std::string  m_pixMgrLocation;                    //!< Location of pixel Manager 
  //const InDetDD::PixelDetectorManager*  m_pixMgr;   //!< Detector Manager
  
  Identifier getChId( Muon::MuonSegment& seg );
  std::vector<Identifier> getvectChId( Muon::MuonSegment& seg );
  int segtrackHitsOverlap( const Trk::Segment *seg1, const Trk::Track *trk);
  //int segtrackHitsOverlap( const Trk::Segment& seg, const Trk::Track& trk);
  Amg::Transform3D getGlobalToStation( Identifier& id );
  
  int numRoIs( Muon::MuonSegment& seg );   
  int numSegHits( Muon::MuonSegment& seg );
  int numTrackHits(const Trk::Track* trk);
  int numSegSegHitShare( Muon::MuonSegment& seg, Muon::MuonSegment &segger );
  /////const Trk::Perigee* createPerigee( const Trk::TrackParameters& pars );
  //ToolHandle<Trk::IExtrapolator>      m_extrapolator; 
  
  bool CUT_segsPerSector(int count);
  bool CUT_hitsOnSegment (const xAOD::MuonSegment* muonseg);
  bool CUT_fitqualityOfSegment (const xAOD::MuonSegment* muonseg);
  //  bool BaseAnalysis::CUT_edgeOfChamber (){}
  //  bool BaseAnalysis::CUT_hitsInNeigbourChamber (){}
  bool CUTS_matchSegTrack(const Trk::Segment *segment, const Trk::Track *track);
  //  bool BaseAnalysis::CUTS_authorSegs() {}
  //bool BaseAnalysis::CUTS_TGCPhiHit() {}
  //bool BaseAnalysis::CUTS_pointingSegTrack () {}
  //bool BaseAnalysis::CUTS_pointingSegs () {}


 private:
  std::string m_TrkTrackCollection; // CombinedMuonTracks or MuonSpectrometerTracks
  //  enum  xAOD::Muon::TrackParticleType m_muonTrackParticleType;
  //enum m_muonTrackParticleType;
  // TrackParticleType m_muonTrackParticleType;  //below with prefix xAOD::Muon::
  //std::string m_TrackParticleCollection;//const xAOD::TrackParticle* idtrk = muon->trackParticle( xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle ); //CombinedMuonTrackParticles or MuonSpectrometerTrackParticles
  int m_minSegPerSector;
  int m_maxSegPerSector;
  double m_fitQualPerSeg;
  int m_minHitsEM;
  int m_minHitsEO;
  int m_minHitsEI;
  int m_minHitsBEE;
  int m_maxHitsBEE;

  /** Pointer to MuonDetectorManager */
  //  const MuonGM::MuonDetectorManager*  m_detMgr;
  
  /** Pointers to the Identifier Helpers */
  //const MdtIdHelper*  m_mdtIdHelper;
  //const CscIdHelper*  m_cscIdHelper;
  //const RpcIdHelper*  m_rpcIdHelper;
  //const TgcIdHelper*  m_tgcIdHelper;


  TTree *newTreeFriend;
  TH1F *hist_nMuons;
  TH1F *hist_nSegments;

  TTree *etree;
  TTree *mtree;
  TTree *stree;
  TTree *sItree;

  TTree *tree;
  TH1D *myHist;

  uint32_t eRunNumber; //! 
  uint32_t eEventNumber; //!
  uint32_t eLumiBlock; //!
  int enMuons; //!

  int nMuons;

  uint32_t mRunNumber; //! 
  uint32_t mEventNumber; //!
  uint32_t mLumiBlock; //!

  int muon_n; //!
  double muon_pt; //! 
  double muon_eta; //! 
  double muon_phi; //! 
  double muon_m; //! 
  double muon_e; //! 
  double muon_charge; //! 
  int muon_type; //! 
  int muon_EnergyLossType; //! 
  int muon_author; //! 
  bool muon_quality; //! 
  bool muon_pass_IDhits; //! 
  bool muon_pass_HighPt; //! 
  int muon_nSegments; //! 
  int muon_trMatch; //!
  uint16_t muon_allAuthors; //!
  bool isAuthorUnknown; //!
  bool isAuthorMuid; //!
  bool isAuthorStaco; //!
  bool isAuthorMuTag; //!
  bool isAuthorMuTagIMO; //!
  bool isAuthorMuidSA; //!
  bool isAuthorMuGirl; //!
  bool isAuthorMuGirlLowBeta; //!
  bool isAuthorCaloTag; //!
  bool isAuthorCaloLikelihood; //!
  bool isAuthorExtrapolateMuonToIP; //!
  int muon_nAuthors; //!

  uint8_t muon_numberOfPrecisionLayers; //!
  uint8_t muon_numberOfPrecisionHoleLayers; //!
  uint8_t muon_numberOfPhiLayers; //!
  uint8_t muon_numberOfPhiHoleLayers; //!
  uint8_t muon_numberOfTriggerEtaLayers; //!
  uint8_t muon_numberOfTriggerEtaHoleLayers; //!
  uint8_t muon_primarySector; //!
  uint8_t muon_secondarySector; //!
  uint8_t muon_innerSmallHits; //!
  uint8_t muon_innerLargeHits; //!
  uint8_t muon_middleSmallHits; //!
  uint8_t muon_middleLargeHits; //!
  uint8_t muon_outerSmallHits; //!
  uint8_t muon_outerLargeHits; //!
  uint8_t muon_extendedSmallHits; //!
  uint8_t muon_extendedLargeHits; //!

  uint8_t muon_innerSmallHoles; //!
  uint8_t muon_innerLargeHoles; //!
  uint8_t muon_middleSmallHoles; //!
  uint8_t muon_middleLargeHoles; //!
  uint8_t muon_outerSmallHoles; //!
  uint8_t muon_outerLargeHoles; //!
  uint8_t muon_extendedSmallHoles; //!
  uint8_t muon_extendedLargeHoles; //!



   float mspectrometerFieldIntegral; //!
   float mscatteringCurvatureSignificance; //!
   float mscatteringNeighbourSignificance; //!
   float mmomentumBalanceSignificance; //!
       /** MuTag parameters */
   float msegmentDeltaEta; //!
   float msegmentDeltaPhi; //!
   float msegmentChi2OverDoF; //!
       /** MuGirl parameter */
   float mt0; //!
   float mbeta; //!
   float mannBarrel; //!
   float mannEndCap; //!
       /** common MuGirl and MuTag parameters */
   float minnAngle; //!
   float mmidAngle; //!
   float mmsInnerMatchChi2; //!
   float mmsInnerMatchDOF; //!
   float mmsOuterMatchChi2; //!
   float mmsOuterMatchDOF; //!
   float mmeanDeltaADCCountsMDT; //!
       /** CaloMuon variables (EnergyLossType is stored separately and retrieved using energyLossType() */
   float mCaloLRLikelihood; //!
   float mCaloMuonIDTag; //!
   float mFSR_CandidateEnergy; //!
   float mEnergyLoss; //!
   float mParamEnergyLoss; //!
   float mMeasEnergyLoss; //!
   float mEnergyLossSigma; //!
   float mParamEnergyLossSigmaPlus; //!
   float mParamEnergyLossSigmaMinus; //!
   float mMeasEnergyLossSigma; //!


   uint32_t sRunNumber; //! 
  uint32_t sEventNumber; //! 
  uint32_t sLumiBlock; //!
  int seg_muon_n; //! 
  int seg_author; //!
  int seg_eta_index; //! 
  int seg_tech; //! 
  int seg_cham_index; //!
  int seg_index; //!
  std::string seg_name; //! 
  std::string seg_name_short; //! 
  bool seg_haveTrkSeg; //! 
  int seg_identifier; //! 
  double seg_x; //! 
  double seg_y; //! 
  double seg_z; //! 
  double seg_px; //! 
  double seg_py; //! 
  double seg_pz; //! 
  double seg_t0; //! 
  double seg_t0_error; //! 
  double seg_chisquare; //! 
  double seg_nDOF; //! 
  double seg_nPrecisionHits; //! 
  double seg_nTrigEtaLayers; //! 
  double seg_nTrigPhiLayers; //! 
  double seg_global_x; //! 
  double seg_global_y; //! 
  double seg_global_z; //! 
  double seg_global_px; //! 
  double seg_global_py; //! 
  double seg_global_pz; //! 
  double seg_local_x; //! 
  double seg_local_y; //! 
  double seg_local_z; //! 
  double seg_local_px; //! 
  double seg_local_py; //! 
  double seg_local_pz; //! 
  double seg_cham_x; //! 
  double seg_cham_y; //! 
  double seg_cham_z; //!
  int seg_nhits; //!
  int seg_ntrackhits; //!
  int seg_track_hitshare; //!
  int seg_hitreq; //!
  bool seg_fitqual; //!
  std::vector<double> seg_vMB_x; //!
  std::vector<double> seg_vMB_y; //!
  std::vector<double> seg_vMB_z; //!
  std::vector<int> seg_hitshare; //!

  double seg_trk_match_id; //!
  double seg_trk_pos_global_x; //!
  double seg_trk_pos_global_y; //!
  double seg_trk_pos_global_z; //!
  double seg_trk_pos_local_x; //!
  double seg_trk_pos_local_y; //!
  double seg_trk_pos_local_z; //!
  double seg_trk_dir_global_x; //!
  double seg_trk_dir_global_y; //!
  double seg_trk_dir_global_z; //!
  double seg_trk_dir_local_x; //!
  double seg_trk_dir_local_y; //!
  double seg_trk_dir_local_z; //!


  uint32_t sIRunNumber; //! 
  uint32_t sIEventNumber; //!
  uint32_t sILumiBlock; //!
 
  int segI_muon_n; //! 
  int segI_author; //!
  int segI_eta_index; //! 
  int segI_tech; //! 
  int segI_cham_index; //!
  int segI_index; //!
  std::string segI_name; //! 
  std::string segI_name_short; //! 
  bool segI_haveTrkSeg; //! 
  int segI_identifier; //! 
  double segI_x; //! 
  double segI_y; //! 
  double segI_z; //! 
  double segI_px; //! 
  double segI_py; //! 
  double segI_pz; //! 
  double segI_t0; //! 
  double segI_t0_error; //! 
  double segI_chisquare; //! 
  double segI_nDOF; //! 
  double segI_nPrecisionHits; //! 
  double segI_nTrigEtaLayers; //! 
  double segI_nTrigPhiLayers; //! 
  double segI_global_x; //! 
  double segI_global_y; //! 
  double segI_global_z; //! 
  double segI_global_px; //! 
  double segI_global_py; //! 
  double segI_global_pz; //! 
  double segI_local_x; //! 
  double segI_local_y; //! 
  double segI_local_z; //! 
  double segI_local_px; //! 
  double segI_local_py; //! 
  double segI_local_pz; //! 
  double segI_cham_x; //! 
  double segI_cham_y; //! 
  double segI_cham_z; //!
  int segI_nhits; //!
  int segI_ntrackhits; //!
  int segI_track_hitshare; //!
  int segI_hitreq; //!
  bool segI_fitqual; //!
  std::vector<double> segI_vMB_x; //!
  std::vector<double> segI_vMB_y; //!
  std::vector<double> segI_vMB_z; //!
  std::vector<int> segI_hitshare; //!


  
  int seg1_index; //!
  int seg2_index; //!
  int seg3_index; //!


  int segger1_author; //!
  int segger1_muon_author; //!
  bool segger1_hitsOnTrack; //!
  
  double segger1_muon_pt; //!
  double segger1_muon_eta; //!
  double segger1_muon_phi; //!

  int segger1_nhits; //!
  int segger1_ntrackhits; //!
  int segger1_track_hitshare;//!
  std::vector<int> segger1_hitshare;//!

  int segger2_author; //!
  int segger2_muon_author; //!
  bool segger2_hitsOnTrack; //!
  
  double segger2_muon_pt; //!
  double segger2_muon_eta; //!
  double segger2_muon_phi; //!

  int segger2_nhits; //!
  int segger2_ntrackhits; //!
  int segger2_track_hitshare;//!
  std::vector<int> segger2_hitshare;//!

  int segger3_author; //!
  int segger3_muon_author; //!
  bool segger3_hitsOnTrack; //!
  
  double segger3_muon_pt; //!
  double segger3_muon_eta; //!
  double segger3_muon_phi; //!

  int segger3_nhits; //!
  int segger3_ntrackhits; //!
  int segger3_track_hitshare;//!
  std::vector<int> segger3_hitshare;//!

  uint32_t cRunNumber; //! 
  uint32_t cEventNumber; //! 
  uint32_t cLumiBlock; //!

  int comb_muon_n; //! 
  std::string comb_type; //! 
  int comb_side; //! 
  int comb_sector; //!
  int seg1_author; //!
  int seg1_eta_index; //! 
  int seg1_tech; //! 
  int seg1_cham_index; //! 
  std::string seg1_name; //! 
  std::string seg1_name_short; //! 
  double seg1_x; //! 
  double seg1_y; //! 
  double seg1_z; //! 
  double seg1_px; //! 
  double seg1_py; //! 
  double seg1_pz; //! 
  double seg1_t0; //! 
  double seg1_t0_error; //! 
  double seg1_chisquare; //! 
  double seg1_nDOF; //! 
  double seg1_nPrecisionHits; //! 
  double seg1_nTrigEtaLayers; //! 
  double seg1_nTrigPhiLayers; //! 
  double seg1_global_x; //! 
  double seg1_global_y; //! 
  double seg1_global_z; //! 
  double seg1_global_px; //! 
  double seg1_global_py; //! 
  double seg1_global_pz; //! 
  double seg1_local_x; //! 
  double seg1_local_y; //! 
  double seg1_local_z; //! 
  double seg1_local_px; //! 
  double seg1_local_py; //! 
  double seg1_local_pz; //! 
  double seg1_cham_x; //! 
  double seg1_cham_y; //! 
  double seg1_cham_z; //! 
  bool seg1_hitreq; //! 
  bool seg1_fitqual; //! 
  int seg2_author; //!
  int seg2_eta_index; //! 
  int seg2_tech; //! 
  int seg2_cham_index; //! 
  std::string seg2_name; //! 
  std::string seg2_name_short; //! 
  double seg2_x; //! 
  double seg2_y; //! 
  double seg2_z; //! 
  double seg2_px; //! 
  double seg2_py; //! 
  double seg2_pz; //! 
  double seg2_t0; //! 
  double seg2_t0_error; //! 
  double seg2_chisquare; //! 
  double seg2_nDOF; //! 
  double seg2_nPrecisionHits; //! 
  double seg2_nTrigEtaLayers; //! 
  double seg2_nTrigPhiLayers; //! 
  double seg2_global_x; //! 
  double seg2_global_y; //! 
  double seg2_global_z; //! 
  double seg2_global_px; //! 
  double seg2_global_py; //! 
  double seg2_global_pz; //! 
  double seg2_local_x; //! 
  double seg2_local_y; //! 
  double seg2_local_z; //! 
  double seg2_local_px; //! 
  double seg2_local_py; //! 
  double seg2_local_pz; //! 
  double seg2_cham_x; //! 
  double seg2_cham_y; //! 
  double seg2_cham_z; //! 
  bool seg2_hitreq; //! 
  bool seg2_fitqual; //! 
  int seg3_author; //!
  int seg3_eta_index; //! 
  int seg3_tech; //! 
  int seg3_cham_index; //! 
  std::string seg3_name; //! 
  std::string seg3_name_short; //! 
  double seg3_x; //! 
  double seg3_y; //! 
  double seg3_z; //! 
  double seg3_px; //! 
  double seg3_py; //! 
  double seg3_pz; //! 
  double seg3_t0; //! 
  double seg3_t0_error; //! 
  double seg3_chisquare; //! 
  double seg3_nDOF; //! 
  double seg3_nPrecisionHits; //! 
  double seg3_nTrigEtaLayers; //! 
  double seg3_nTrigPhiLayers; //! 
  double seg3_global_x; //! 
  double seg3_global_y; //! 
  double seg3_global_z; //! 
  double seg3_global_px; //! 
  double seg3_global_py; //! 
  double seg3_global_pz; //! 
  double seg3_local_x; //! 
  double seg3_local_y; //! 
  double seg3_local_z; //! 
  double seg3_local_px; //! 
  double seg3_local_py; //! 
  double seg3_local_pz; //! 
  double seg3_cham_x; //! 
  double seg3_cham_y; //! 
  double seg3_cham_z; //! 
  bool seg3_hitreq; //! 
  bool seg3_fitqual; //! 


  double segger1_trk_match_id_count; //!
  double segger1_hitavg_global_x; //!
  double segger1_hitavg_global_y; //!
  double segger1_hitavg_global_z; //!
  double segger1_hitavg_local_x; //!
  double segger1_hitavg_local_y; //!
  double segger1_hitavg_local_z; //!
  double segger1_hitavg_global_px; //!
  double segger1_hitavg_global_py; //!
  double segger1_hitavg_global_pz; //!
  double segger1_hitavg_local_px; //!
  double segger1_hitavg_local_py; //!
  double segger1_hitavg_local_pz; //!

  double segger2_trk_match_id_count; //!
  double segger2_hitavg_global_x; //!
  double segger2_hitavg_global_y; //!
  double segger2_hitavg_global_z; //!
  double segger2_hitavg_local_x; //!
  double segger2_hitavg_local_y; //!
  double segger2_hitavg_local_z; //!
  double segger2_hitavg_global_px; //!
  double segger2_hitavg_global_py; //!
  double segger2_hitavg_global_pz; //!
  double segger2_hitavg_local_px; //!
  double segger2_hitavg_local_py; //!
  double segger2_hitavg_local_pz; //!

  double segger3_trk_match_id_count; //!
  double segger3_hitavg_global_x; //!
  double segger3_hitavg_global_y; //!
  double segger3_hitavg_global_z; //!
  double segger3_hitavg_local_x; //!
  double segger3_hitavg_local_y; //!
  double segger3_hitavg_local_z; //!
  double segger3_hitavg_global_px; //!
  double segger3_hitavg_global_py; //!
  double segger3_hitavg_global_pz; //!
  double segger3_hitavg_local_px; //!
  double segger3_hitavg_local_py; //!
  double segger3_hitavg_local_pz; //!
    

  double vx_x; //!
  double vx_y; //!
  double vx_z; //!
  double vx_chiSquared; //!
  double vx_nDOF; //!



};

#endif //> !DESDMANALYSIS_BASEANALYSIS_H
