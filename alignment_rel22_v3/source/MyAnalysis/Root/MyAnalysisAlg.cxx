// DESDMAnalysis includes
#include <MyAnalysis/MyAnalysisAlg.h>

#include "xAODEventInfo/EventInfo.h"
#include "xAODParticleEvent/IParticleLink.h"

#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODMuon/MuonSegment.h"
#include "xAODMuon/MuonSegmentContainer.h"
#include "xAODMuon/MuonSegmentAuxContainer.h"

#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/Vertex.h"

#include "TrkSurfaces/PerigeeSurface.h"
#include "TrkExInterfaces/IExtrapolator.h"

#include "muonEvent/Muon.h"
#include "MuonSegment/MuonSegment.h"
#include "TrkMeasurementBase/MeasurementBase.h"
#include "Identifier/Identifier.h"
#include "Identifier/IdentifierHash.h"
// #include "MuonRecHelperTools/MuonEDMHelperTool.h"


#include "AthLinks/ElementLink.h"
#include "AthContainers/DataVector.h"
#include "AtlasDetDescr/AtlasDetectorID.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"

#include <iostream>
#include <string>

// idhelpers
#include "Identifier/Identifier.h"
#include "InDetIdentifier/PixelID.h"
#include "InDetIdentifier/SCT_ID.h"
#include "InDetIdentifier/TRT_ID.h"
#include "MuonIdHelpers/MdtIdHelper.h"
#include "MuonIdHelpers/CscIdHelper.h"
#include "MuonIdHelpers/RpcIdHelper.h"
#include "MuonIdHelpers/TgcIdHelper.h"
#include "MuonReadoutGeometry/MuonDetectorManager.h"
#include "MuonReadoutGeometry/MdtReadoutElement.h"
#include "MuonReadoutGeometry/RpcReadoutElement.h"
#include "MuonReadoutGeometry/CscReadoutElement.h"
#include "MuonReadoutGeometry/TgcReadoutElement.h"

MyAnalysisAlg::MyAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthHistogramAlgorithm( name, pSvcLocator ),
										  m_TrkTrackCollection("CombinedMuonTracks"), // //MuonSpectrometerTracks
										  m_minSegPerSector(3),
										  m_maxSegPerSector(5),
										  m_fitQualPerSeg(10.),
										  m_minHitsEM(5),
										  m_minHitsEO(5),
										  m_minHitsEI(7),
										  m_minHitsBEE(5),
										  m_maxHitsBEE(12)
//m_helperTool("Muon::MuonEDMHelperTool/MuonEDMHelperTool")
{
  // Python based properties that can be steered via job options
  declareProperty( "TrackCollection_key_name", m_TrkTrackCollection = "CombinedMuonTracks"); //MuonSpectrometerTracks //CombinedMuonTracks
  declareProperty( "minimum_number_of_segments_per_sector", m_minSegPerSector);
  declareProperty( "maximum_number_of_segments_per_sector", m_maxSegPerSector);
  declareProperty( "maximum_allowed_value_for_fitquality_chi2_over_NDOF_of_segment", m_fitQualPerSeg);

  // EM EO EI BEE hit selection (BEE has min and max requirements)
  declareProperty( "minimum_number_of_precision_hits_for_EM_chamber", m_minHitsEM);
  declareProperty( "minimum_number_of_precision_hits_for_EO_chamber", m_minHitsEO);
  declareProperty( "minimum_number_of_precision_hits_for_EI_chamber", m_minHitsEI);
  declareProperty( "minimum_number_of_precision_hits_for_BEE_chamber", m_minHitsBEE);
  declareProperty( "maximum_number_of_precision_hits_for_BEE_chamber", m_maxHitsBEE);

  //declareProperty("MuonEDMHelperTool", m_helperTool);
}


MyAnalysisAlg::~MyAnalysisAlg() {}

// Initialize/retrieve all tools
StatusCode MyAnalysisAlg::initialize() {
  ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
  ServiceHandle<StoreGateSvc> detectorStoreSvc("DetectorStore", name());
  // Retrieve DetectorStoreSvc
  if (!detectorStoreSvc.retrieve().isSuccess()) {
    ATH_MSG_FATAL("Cannot get DetectorStoreSvc.");
    return(StatusCode::FAILURE);
  }

  // retrieve MuonDetectorManager
  if(detectorStoreSvc->retrieve(m_detMgr,"Muon").isFailure()) {
    ATH_MSG_ERROR("Could not find the MuonGeoModel Manager! ");
    return StatusCode::FAILURE;
  }

  // retrieve Pixel information
  //  if(detectorStoreSvc->retrieve(m_pixMgr,"Pixel").isFailure()) {
  //  ATH_MSG_ERROR("Could not find the Pixel Manager! ");
  //  return StatusCode::FAILURE;
  //}

  // initialize MuonIdHelpers
  if (m_detMgr) {
    m_mdtIdHelper = m_detMgr->mdtIdHelper();
    m_cscIdHelper = m_detMgr->cscIdHelper();
    m_rpcIdHelper = m_detMgr->rpcIdHelper();
    m_tgcIdHelper = m_detMgr->tgcIdHelper();
  } else {
    m_mdtIdHelper = 0;
    m_cscIdHelper = 0;
    m_rpcIdHelper = 0;
    m_tgcIdHelper = 0;
  }

  // Hist below isn't needed - was a test that can be removed
  // Gives an exaple of storing histogram directly
  // CHECK( histSvc.retrieve() );
  // myHist = new TH1D("myHist","myHist",10,0,10);
  // histSvc->regHist("/MYSTREAM/myHist",myHist).ignore(); //or check the statuscode

  // Ntuple that stores event information
  etree = new TTree("etree","etree");
  CHECK( histSvc->regTree("/ANOTHERSTREAM/etree",etree) );

  // Ntuple that stores muon information
  mtree = new TTree("mtree","mtree");
  CHECK( histSvc->regTree("/ANOTHERSTREAM/mtree",mtree) );

  // Ntuple that stores information about segments from muon
  stree = new TTree("stree","stree");
  CHECK( histSvc->regTree("/ANOTHERSTREAM/stree",stree) );

  // Ntuple that stores information about all segments directly from container
  sItree = new TTree("sItree","sItree");
  CHECK( histSvc->regTree("/ANOTHERSTREAM/sItree",sItree) );

  // Ntuple that is primarily used for sagitta analysis - stores information about the three relevant segments
  tree = new TTree("tree","tree");
  CHECK( histSvc->regTree("/ANOTHERSTREAM/tree",tree) );

  etree->Branch("enMuons", &enMuons);
  etree->Branch("eRunNumber", &eRunNumber);
  etree->Branch("eEventNumber", &eEventNumber);
  etree->Branch("eLumiBlock", &eLumiBlock);

  etree->Branch("vx_x", &vx_x);
  etree->Branch("vx_y", &vx_y);
  etree->Branch("vx_z", &vx_z);
  etree->Branch("vx_chiSquared", &vx_chiSquared);
  etree->Branch("vx_nDOF", &vx_nDOF);
  
  mtree->Branch("mRunNumber", &mRunNumber);
  mtree->Branch("mEventNumber", &mEventNumber);
  mtree->Branch("mLumiBlock", &mLumiBlock);

  mtree->Branch("muon_n", &muon_n);
  mtree->Branch("muon_pt", &muon_pt);
  mtree->Branch("muon_eta", &muon_eta);
  mtree->Branch("muon_phi", &muon_phi);
  mtree->Branch("muon_m", &muon_m);
  mtree->Branch("muon_e", &muon_e);
  mtree->Branch("muon_charge", &muon_charge);
  mtree->Branch("muon_type", &muon_type);
  mtree->Branch("muon_EnergyLossType", &muon_EnergyLossType);
  mtree->Branch("muon_author", &muon_author);
  mtree->Branch("muon_quality", &muon_quality);
  mtree->Branch("muon_pass_IDhits", &muon_pass_IDhits);
  mtree->Branch("muon_pass_HighPt", &muon_pass_HighPt);
  mtree->Branch("muon_nSegments", &muon_nSegments);
  mtree->Branch("muon_trMatch", &muon_trMatch);
  mtree->Branch("muon_allAuthors", &muon_allAuthors);

  mtree->Branch("isAuthorUnknown", &isAuthorUnknown);
  mtree->Branch("isAuthorMuid", &isAuthorMuid);
  mtree->Branch("isAuthorStaco", &isAuthorStaco);
  mtree->Branch("isAuthorMuTag", &isAuthorMuTag);
  mtree->Branch("isAuthorMuTagIMO", &isAuthorMuTagIMO);
  mtree->Branch("isAuthorMuidSA", &isAuthorMuidSA);
  mtree->Branch("isAuthorMuGirl", &isAuthorMuGirl);
  mtree->Branch("isAuthorMuGirlLowBeta", &isAuthorMuGirlLowBeta);
  mtree->Branch("isAuthorCaloTag", &isAuthorCaloTag);
  mtree->Branch("isAuthorCaloLikelihood", &isAuthorCaloLikelihood);
  mtree->Branch("isAuthorExtrapolateMuonToIP", &isAuthorExtrapolateMuonToIP);
  mtree->Branch("muon_nAuthors", &muon_nAuthors);

  mtree->Branch("muon_numberOfPrecisionLayers", &muon_numberOfPrecisionLayers);
  mtree->Branch("muon_numberOfPrecisionHoleLayers", &muon_numberOfPrecisionHoleLayers);
  mtree->Branch("muon_numberOfPhiLayers", &muon_numberOfPhiLayers);
  mtree->Branch("muon_numberOfPhiHoleLayers", &muon_numberOfPhiHoleLayers);
  mtree->Branch("muon_numberOfTriggerEtaLayers", &muon_numberOfTriggerEtaLayers);
  mtree->Branch("muon_numberOfTriggerEtaHoleLayers", &muon_numberOfTriggerEtaHoleLayers);
  mtree->Branch("muon_primarySector", &muon_primarySector);
  mtree->Branch("muon_secondarySector", &muon_secondarySector);
  mtree->Branch("muon_innerSmallHits", &muon_innerSmallHits);
  mtree->Branch("muon_innerLargeHits", &muon_innerLargeHits);
  mtree->Branch("muon_middleSmallHits", &muon_middleSmallHits);
  mtree->Branch("muon_middleLargeHits", &muon_middleLargeHits);
  mtree->Branch("muon_outerSmallHits", &muon_outerSmallHits);
  mtree->Branch("muon_outerLargeHits", &muon_outerLargeHits);
  mtree->Branch("muon_extendedSmallHits", &muon_extendedSmallHits);
  mtree->Branch("muon_extendedLargeHits", &muon_extendedLargeHits);
  mtree->Branch("muon_innerSmallHoles", &muon_innerSmallHoles);
  mtree->Branch("muon_innerLargeHoles", &muon_innerLargeHoles);
  mtree->Branch("muon_middleSmallHoles", &muon_middleSmallHoles);
  mtree->Branch("muon_middleLargeHoles", &muon_middleLargeHoles);
  mtree->Branch("muon_outerSmallHoles", &muon_outerSmallHoles);
  mtree->Branch("muon_outerLargeHoles", &muon_outerLargeHoles);
  mtree->Branch("muon_extendedSmallHoles", &muon_extendedSmallHoles);
  mtree->Branch("muon_extendedLargeHoles", &muon_extendedLargeHoles);


  mtree->Branch("mspectrometerFieldIntegral", &mspectrometerFieldIntegral);
  mtree->Branch("mscatteringCurvatureSignificance", &mscatteringCurvatureSignificance);
  mtree->Branch("mscatteringNeighbourSignificance", &mscatteringNeighbourSignificance);
  mtree->Branch("mmomentumBalanceSignificance", &mmomentumBalanceSignificance);
  /** MuTag parameters */
  mtree->Branch("msegmentDeltaEta", &msegmentDeltaEta);
  mtree->Branch("msegmentDeltaPhi", &msegmentDeltaPhi);
  mtree->Branch("msegmentChi2OverDoF", &msegmentChi2OverDoF);
  /** MuGirl parameter */
  mtree->Branch("mt0", &mt0);
  mtree->Branch("mbeta", &mbeta);
  mtree->Branch("mannBarrel", &mannBarrel);
  mtree->Branch("mannEndCap", &mannEndCap);
  /** common MuGirl and MuTag parameters */
  mtree->Branch("minnAngle", &minnAngle);
  mtree->Branch("mmidAngle", &mmidAngle);
  mtree->Branch("mmsInnerMatchChi2", &mmsInnerMatchChi2);
  mtree->Branch("mmsInnerMatchDOF", &mmsInnerMatchDOF);
  mtree->Branch("mmsOuterMatchChi2", &mmsOuterMatchChi2);
  mtree->Branch("mmsOuterMatchDOF", &mmsOuterMatchDOF);
  mtree->Branch("mmeanDeltaADCCountsMDT", &mmeanDeltaADCCountsMDT);
  /** CaloMuon variables (EnergyLossType is stored separately and retrieved using energyLossType() */
  mtree->Branch("mCaloLRLikelihood", &mCaloLRLikelihood);
  mtree->Branch("mCaloMuonIDTag", &mCaloMuonIDTag);
  mtree->Branch("mFSR_CandidateEnergy", &mFSR_CandidateEnergy);
  mtree->Branch("mEnergyLoss", &mEnergyLoss);
  mtree->Branch("mParamEnergyLossmPara", &mEnergyLoss);
  mtree->Branch("mMeasEnergyLoss", &mMeasEnergyLoss);
  mtree->Branch("mEnergyLossSigma", &mEnergyLossSigma);
  mtree->Branch("mParamEnergyLossSigmaPlus", &mParamEnergyLossSigmaPlus);
  mtree->Branch("mParamEnergyLossSigmaMinus", &mParamEnergyLossSigmaMinus);
  mtree->Branch("mMeasEnergyLossSigma", &mMeasEnergyLossSigma);

  stree->Branch("sRunNumber", &sRunNumber);
  stree->Branch("sEventNumber", &sEventNumber);
  stree->Branch("sLumiBlock", &sLumiBlock);

  stree->Branch("seg_muon_n", &seg_muon_n);
  stree->Branch("seg_author", &seg_author);
  stree->Branch("seg_eta_index", &seg_eta_index);
  stree->Branch("seg_tech", &seg_tech);
  stree->Branch("seg_cham_index", &seg_cham_index);
  stree->Branch("seg_name", &seg_name);
  stree->Branch("seg_name_short", &seg_name_short);
  stree->Branch("seg_haveTrkSeg", &seg_haveTrkSeg);
  stree->Branch("seg_identifier", &seg_identifier);
  stree->Branch("seg_index", &seg_index);
  stree->Branch("seg_x", &seg_x);
  stree->Branch("seg_y", &seg_y);
  stree->Branch("seg_z", &seg_z);
  stree->Branch("seg_px", &seg_px);
  stree->Branch("seg_py", &seg_py);
  stree->Branch("seg_pz", &seg_pz);
  stree->Branch("seg_t0", &seg_t0);
  stree->Branch("seg_t0_error", &seg_t0_error);
  stree->Branch("seg_chisquare", &seg_chisquare);
  stree->Branch("seg_nDOF", &seg_nDOF);
  stree->Branch("seg_nPrecisionHits", &seg_nPrecisionHits);
  stree->Branch("seg_nTrigEtaLayers", &seg_nTrigEtaLayers);
  stree->Branch("seg_nTrigPhiLayers", &seg_nTrigPhiLayers);
  stree->Branch("seg_global_x", &seg_global_x);
  stree->Branch("seg_global_y", &seg_global_y);
  stree->Branch("seg_global_z", &seg_global_z);
  stree->Branch("seg_global_px", &seg_global_px);
  stree->Branch("seg_global_py", &seg_global_py);
  stree->Branch("seg_global_pz", &seg_global_pz);
  stree->Branch("seg_local_x", &seg_local_x);
  stree->Branch("seg_local_y", &seg_local_y);
  stree->Branch("seg_local_z", &seg_local_z);
  stree->Branch("seg_local_px", &seg_local_px);
  stree->Branch("seg_local_py", &seg_local_py);
  stree->Branch("seg_local_pz", &seg_local_pz);
  stree->Branch("seg_cham_x", &seg_cham_x);
  stree->Branch("seg_cham_y", &seg_cham_y);
  stree->Branch("seg_cham_z", &seg_cham_z);
  stree->Branch("seg_vMB_x", &seg_vMB_x);
  stree->Branch("seg_vMB_y", &seg_vMB_y);
  stree->Branch("seg_vMB_z", &seg_vMB_z);
  stree->Branch("seg_nhits", &seg_nhits);
  stree->Branch("seg_ntrackhits", &seg_ntrackhits);
  stree->Branch("seg_track_hitshare", &seg_track_hitshare);
  stree->Branch("seg_hitshare", &seg_hitshare);
  stree->Branch("seg_hitreq", &seg_hitreq);
  stree->Branch("seg_fitqual", &seg_fitqual);

  stree->Branch("seg_trk_match_id", &seg_trk_match_id);
  stree->Branch("seg_trk_pos_global_x", &seg_trk_pos_global_x);
  stree->Branch("seg_trk_pos_global_y", &seg_trk_pos_global_y);
  stree->Branch("seg_trk_pos_global_z", &seg_trk_pos_global_z);
  stree->Branch("seg_trk_pos_local_x", &seg_trk_pos_local_x);
  stree->Branch("seg_trk_pos_local_y", &seg_trk_pos_local_y);
  stree->Branch("seg_trk_pos_local_z", &seg_trk_pos_local_z);
  stree->Branch("seg_trk_dir_global_x", &seg_trk_dir_global_x);
  stree->Branch("seg_trk_dir_global_y", &seg_trk_dir_global_y);
  stree->Branch("seg_trk_dir_global_z", &seg_trk_dir_global_z);
  stree->Branch("seg_trk_dir_local_x", &seg_trk_dir_local_x);
  stree->Branch("seg_trk_dir_local_y", &seg_trk_dir_local_y);
  stree->Branch("seg_trk_dir_local_z", &seg_trk_dir_local_z);

  sItree->Branch("sIRunNumber", &sIRunNumber);
  sItree->Branch("sIEventNumber", &sIEventNumber);
  stree->Branch("sILumiBlock", &sILumiBlock);

  sItree->Branch("segI_muon_n", &segI_muon_n);
  sItree->Branch("segI_author", &segI_author);
  sItree->Branch("segI_eta_index", &segI_eta_index);
  sItree->Branch("segI_tech", &segI_tech);
  sItree->Branch("segI_cham_index", &segI_cham_index);
  sItree->Branch("segI_name", &segI_name);
  sItree->Branch("segI_name_short", &segI_name_short);
  sItree->Branch("segI_haveTrkSeg", &segI_haveTrkSeg);
  sItree->Branch("segI_identifier", &segI_identifier);
  sItree->Branch("segI_index", &segI_index);
  sItree->Branch("segI_x", &segI_x);
  sItree->Branch("segI_y", &segI_y);
  sItree->Branch("segI_z", &segI_z);
  sItree->Branch("segI_px", &segI_px);
  sItree->Branch("segI_py", &segI_py);
  sItree->Branch("segI_pz", &segI_pz);
  sItree->Branch("segI_t0", &segI_t0);
  sItree->Branch("segI_t0_error", &segI_t0_error);
  sItree->Branch("segI_chisquare", &segI_chisquare);
  sItree->Branch("segI_nDOF", &segI_nDOF);
  sItree->Branch("segI_nPrecisionHits", &segI_nPrecisionHits);
  sItree->Branch("segI_nTrigEtaLayers", &segI_nTrigEtaLayers);
  sItree->Branch("segI_nTrigPhiLayers", &segI_nTrigPhiLayers);
  sItree->Branch("segI_global_x", &segI_global_x);
  sItree->Branch("segI_global_y", &segI_global_y);
  sItree->Branch("segI_global_z", &segI_global_z);
  sItree->Branch("segI_global_px", &segI_global_px);
  sItree->Branch("segI_global_py", &segI_global_py);
  sItree->Branch("segI_global_pz", &segI_global_pz);
  sItree->Branch("segI_local_x", &segI_local_x);
  sItree->Branch("segI_local_y", &segI_local_y);
  sItree->Branch("segI_local_z", &segI_local_z);
  sItree->Branch("segI_local_px", &segI_local_px);
  sItree->Branch("segI_local_py", &segI_local_py);
  sItree->Branch("segI_local_pz", &segI_local_pz);
  sItree->Branch("segI_cham_x", &segI_cham_x);
  sItree->Branch("segI_cham_y", &segI_cham_y);
  sItree->Branch("segI_cham_z", &segI_cham_z);
  sItree->Branch("segI_vMB_x", &segI_vMB_x);
  sItree->Branch("segI_vMB_y", &segI_vMB_y);
  sItree->Branch("segI_vMB_z", &segI_vMB_z);
  sItree->Branch("segI_nhits", &segI_nhits);
  sItree->Branch("segI_ntrackhits", &segI_ntrackhits);
  sItree->Branch("segI_track_hitshare", &segI_track_hitshare);
  sItree->Branch("segI_hitshare", &segI_hitshare);
  sItree->Branch("segI_hitreq", &segI_hitreq);
  sItree->Branch("segI_fitqual", &segI_fitqual);

  tree->Branch("vx_x", &vx_x);
  tree->Branch("vx_y", &vx_y);
  tree->Branch("vx_z", &vx_z);
  tree->Branch("vx_chiSquared", &vx_chiSquared);
  tree->Branch("vx_nDOF", &vx_nDOF);

  tree->Branch("seg1_index", &seg1_index);
  tree->Branch("seg2_index", &seg2_index);
  tree->Branch("seg3_index", &seg3_index);

  tree->Branch("segger1_author", &segger1_author);
  tree->Branch("segger1_muon_author", &segger1_muon_author);
  tree->Branch("segger1_hitsOnTrack", &segger1_hitsOnTrack);
  tree->Branch("segger1_muon_pt", &segger1_muon_pt);
  tree->Branch("segger1_muon_eta", &segger1_muon_eta);
  tree->Branch("segger1_muon_phi", &segger1_muon_phi);
  tree->Branch("segger2_author", &segger2_author);
  tree->Branch("segger2_muon_author", &segger2_muon_author);
  tree->Branch("segger2_hitsOnTrack", &segger2_hitsOnTrack);
  tree->Branch("segger2_muon_pt", &segger2_muon_pt);
  tree->Branch("segger2_muon_eta", &segger2_muon_eta);
  tree->Branch("segger2_muon_phi", &segger2_muon_phi);
  tree->Branch("segger3_author", &segger2_author);
  tree->Branch("segger3_muon_author", &segger2_muon_author);
  tree->Branch("segger3_hitsOnTrack", &segger2_hitsOnTrack);
  tree->Branch("segger3_muon_pt", &segger2_muon_pt);
  tree->Branch("segger3_muon_eta", &segger2_muon_eta);
  tree->Branch("segger3_muon_phi", &segger2_muon_phi);
  tree->Branch("segger1_nhits", &segger1_nhits);
  tree->Branch("segger1_ntrackhits", &segger1_ntrackhits);
  tree->Branch("segger1_track_hitshare", &segger1_track_hitshare);
  tree->Branch("segger1_hitshare", &segger1_hitshare);
  tree->Branch("segger2_nhits", &segger2_nhits);
  tree->Branch("segger2_ntrackhits", &segger2_ntrackhits);
  tree->Branch("segger2_track_hitshare", &segger2_track_hitshare);
  tree->Branch("segger2_hitshare", &segger2_hitshare);
  tree->Branch("segger3_nhits", &segger3_nhits);
  tree->Branch("segger3_ntrackhits", &segger3_ntrackhits);
  tree->Branch("segger3_track_hitshare", &segger3_track_hitshare);
  tree->Branch("segger3_hitshare", &segger3_hitshare);

  tree->Branch("cRunNumber", &cRunNumber);
  tree->Branch("cEventNumber", &cEventNumber);
  tree->Branch("cLumiBlock", &cLumiBlock);

  tree->Branch("comb_muon_n", &comb_muon_n);
  tree->Branch("comb_type", &comb_type);
  tree->Branch("comb_side", &comb_side);
  tree->Branch("comb_sector", &comb_sector);
  tree->Branch("seg1_author", &seg1_author);
  tree->Branch("seg1_eta_index", &seg1_eta_index);
  tree->Branch("seg1_tech", &seg1_tech);
  tree->Branch("seg1_cham_index", &seg1_cham_index);
  tree->Branch("seg1_name", &seg1_name);
  tree->Branch("seg1_name_short", &seg1_name_short);
  tree->Branch("seg1_x", &seg1_x);
  tree->Branch("seg1_y", &seg1_y);
  tree->Branch("seg1_z", &seg1_z);
  tree->Branch("seg1_px", &seg1_px);
  tree->Branch("seg1_py", &seg1_py);
  tree->Branch("seg1_pz", &seg1_pz);
  tree->Branch("seg1_t0", &seg1_t0);
  tree->Branch("seg1_t0_error", &seg1_t0_error);
  tree->Branch("seg1_chisquare", &seg1_chisquare);
  tree->Branch("seg1_nDOF", &seg1_nDOF);
  tree->Branch("seg1_nPrecisionHits", &seg1_nPrecisionHits);
  tree->Branch("seg1_nTrigEtaLayers", &seg1_nTrigEtaLayers);
  tree->Branch("seg1_nTrigPhiLayers", &seg1_nTrigPhiLayers);
  tree->Branch("seg1_global_x", &seg1_global_x);
  tree->Branch("seg1_global_y", &seg1_global_y);
  tree->Branch("seg1_global_z", &seg1_global_z);
  tree->Branch("seg1_global_px", &seg1_global_px);
  tree->Branch("seg1_global_py", &seg1_global_py);
  tree->Branch("seg1_global_pz", &seg1_global_pz);
  tree->Branch("seg1_local_x", &seg1_local_x);
  tree->Branch("seg1_local_y", &seg1_local_y);
  tree->Branch("seg1_local_z", &seg1_local_z);
  tree->Branch("seg1_local_px", &seg1_local_px);
  tree->Branch("seg1_local_py", &seg1_local_py);
  tree->Branch("seg1_local_pz", &seg1_local_pz);
  tree->Branch("seg1_cham_x", &seg1_cham_x);
  tree->Branch("seg1_cham_y", &seg1_cham_y);
  tree->Branch("seg1_cham_z", &seg1_cham_z);
  tree->Branch("seg1_hitreq", &seg1_hitreq);
  tree->Branch("seg1_fitqual", &seg1_fitqual);
  tree->Branch("seg2_author", &seg2_author);
  tree->Branch("seg2_eta_index", &seg2_eta_index);
  tree->Branch("seg2_tech", &seg2_tech);
  tree->Branch("seg2_cham_index", &seg2_cham_index);
  tree->Branch("seg2_name", &seg2_name);
  tree->Branch("seg2_name_short", &seg2_name_short);
  tree->Branch("seg2_x", &seg2_x);
  tree->Branch("seg2_y", &seg2_y);
  tree->Branch("seg2_z", &seg2_z);
  tree->Branch("seg2_px", &seg2_px);
  tree->Branch("seg2_py", &seg2_py);
  tree->Branch("seg2_pz", &seg2_pz);
  tree->Branch("seg2_t0", &seg2_t0);
  tree->Branch("seg2_t0_error", &seg2_t0_error);
  tree->Branch("seg2_chisquare", &seg2_chisquare);
  tree->Branch("seg2_nDOF", &seg2_nDOF);
  tree->Branch("seg2_nPrecisionHits", &seg2_nPrecisionHits);
  tree->Branch("seg2_nTrigEtaLayers", &seg2_nTrigEtaLayers);
  tree->Branch("seg2_nTrigPhiLayers", &seg2_nTrigPhiLayers);
  tree->Branch("seg2_global_x", &seg2_global_x);
  tree->Branch("seg2_global_y", &seg2_global_y);
  tree->Branch("seg2_global_z", &seg2_global_z);
  tree->Branch("seg2_global_px", &seg2_global_px);
  tree->Branch("seg2_global_py", &seg2_global_py);
  tree->Branch("seg2_global_pz", &seg2_global_pz);
  tree->Branch("seg2_local_x", &seg2_local_x);
  tree->Branch("seg2_local_y", &seg2_local_y);
  tree->Branch("seg2_local_z", &seg2_local_z);
  tree->Branch("seg2_local_px", &seg2_local_px);
  tree->Branch("seg2_local_py", &seg2_local_py);
  tree->Branch("seg2_local_pz", &seg2_local_pz);
  tree->Branch("seg2_cham_x", &seg2_cham_x);
  tree->Branch("seg2_cham_y", &seg2_cham_y);
  tree->Branch("seg2_cham_z", &seg2_cham_z);
  tree->Branch("seg2_hitreq", &seg2_hitreq);
  tree->Branch("seg2_fitqual", &seg2_fitqual);
  tree->Branch("seg3_author", &seg3_author);
  tree->Branch("seg3_eta_index", &seg3_eta_index);
  tree->Branch("seg3_tech", &seg3_tech);
  tree->Branch("seg3_cham_index", &seg3_cham_index);
  tree->Branch("seg3_name", &seg3_name);
  tree->Branch("seg3_name_short", &seg3_name_short);
  tree->Branch("seg3_x", &seg3_x);
  tree->Branch("seg3_y", &seg3_y);
  tree->Branch("seg3_z", &seg3_z);
  tree->Branch("seg3_px", &seg3_px);
  tree->Branch("seg3_py", &seg3_py);
  tree->Branch("seg3_pz", &seg3_pz);
  tree->Branch("seg3_t0", &seg3_t0);
  tree->Branch("seg3_t0_error", &seg3_t0_error);
  tree->Branch("seg3_chisquare", &seg3_chisquare);
  tree->Branch("seg3_nDOF", &seg3_nDOF);
  tree->Branch("seg3_nPrecisionHits", &seg3_nPrecisionHits);
  tree->Branch("seg3_nTrigEtaLayers", &seg3_nTrigEtaLayers);
  tree->Branch("seg3_nTrigPhiLayers", &seg3_nTrigPhiLayers);
  tree->Branch("seg3_global_x", &seg3_global_x);
  tree->Branch("seg3_global_y", &seg3_global_y);
  tree->Branch("seg3_global_z", &seg3_global_z);
  tree->Branch("seg3_global_px", &seg3_global_px);
  tree->Branch("seg3_global_py", &seg3_global_py);
  tree->Branch("seg3_global_pz", &seg3_global_pz);
  tree->Branch("seg3_local_x", &seg3_local_x);
  tree->Branch("seg3_local_y", &seg3_local_y);
  tree->Branch("seg3_local_z", &seg3_local_z);
  tree->Branch("seg3_local_px", &seg3_local_px);
  tree->Branch("seg3_local_py", &seg3_local_py);
  tree->Branch("seg3_local_pz", &seg3_local_pz);
  tree->Branch("seg3_cham_x", &seg3_cham_x);
  tree->Branch("seg3_cham_y", &seg3_cham_y);
  tree->Branch("seg3_cham_z", &seg3_cham_z);
  tree->Branch("seg3_hitreq", &seg3_hitreq);
  tree->Branch("seg3_fitqual", &seg3_fitqual);

  tree->Branch("segger1_trk_match_id_count", &segger1_trk_match_id_count);
  tree->Branch("segger1_hitavg_global_x", &segger1_hitavg_global_x);
  tree->Branch("segger1_hitavg_global_y", &segger1_hitavg_global_y);
  tree->Branch("segger1_hitavg_global_z", &segger1_hitavg_global_z);
  tree->Branch("segger1_hitavg_local_x", &segger1_hitavg_local_x);
  tree->Branch("segger1_hitavg_local_y", &segger1_hitavg_local_y);
  tree->Branch("segger1_hitavg_local_z", &segger1_hitavg_local_z);
  tree->Branch("segger1_hitavg_global_px", &segger1_hitavg_global_px);
  tree->Branch("segger1_hitavg_global_py", &segger1_hitavg_global_py);
  tree->Branch("segger1_hitavg_global_pz", &segger1_hitavg_global_pz);
  tree->Branch("segger1_hitavg_local_px", &segger1_hitavg_local_px);
  tree->Branch("segger1_hitavg_local_py", &segger1_hitavg_local_py);
  tree->Branch("segger1_hitavg_local_pz", &segger1_hitavg_local_pz);

  tree->Branch("segger2_trk_match_id_count", &segger2_trk_match_id_count);
  tree->Branch("segger2_hitavg_global_x", &segger2_hitavg_global_x);
  tree->Branch("segger2_hitavg_global_y", &segger2_hitavg_global_y);
  tree->Branch("segger2_hitavg_global_z", &segger2_hitavg_global_z);
  tree->Branch("segger2_hitavg_local_x", &segger2_hitavg_local_x);
  tree->Branch("segger2_hitavg_local_y", &segger2_hitavg_local_y);
  tree->Branch("segger2_hitavg_local_z", &segger2_hitavg_local_z);
  tree->Branch("segger2_hitavg_global_px", &segger2_hitavg_global_px);
  tree->Branch("segger2_hitavg_global_py", &segger2_hitavg_global_py);
  tree->Branch("segger2_hitavg_global_pz", &segger2_hitavg_global_pz);
  tree->Branch("segger2_hitavg_local_px", &segger2_hitavg_local_px);
  tree->Branch("segger2_hitavg_local_py", &segger2_hitavg_local_py);
  tree->Branch("segger2_hitavg_local_pz", &segger2_hitavg_local_pz);

  tree->Branch("segger3_trk_match_id_count", &segger3_trk_match_id_count);
  tree->Branch("segger3_hitavg_global_x", &segger3_hitavg_global_x);
  tree->Branch("segger3_hitavg_global_y", &segger3_hitavg_global_y);
  tree->Branch("segger3_hitavg_global_z", &segger3_hitavg_global_z);
  tree->Branch("segger3_hitavg_local_x", &segger3_hitavg_local_x);
  tree->Branch("segger3_hitavg_local_y", &segger3_hitavg_local_y);
  tree->Branch("segger3_hitavg_local_z", &segger3_hitavg_local_z);
  tree->Branch("segger3_hitavg_global_px", &segger3_hitavg_global_px);
  tree->Branch("segger3_hitavg_global_py", &segger3_hitavg_global_py);
  tree->Branch("segger3_hitavg_global_pz", &segger3_hitavg_global_pz);
  tree->Branch("segger3_hitavg_local_px", &segger3_hitavg_local_px);
  tree->Branch("segger3_hitavg_local_py", &segger3_hitavg_local_py);
  tree->Branch("segger3_hitavg_local_pz", &segger3_hitavg_local_pz);
    
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //comb1 for A and C side (this is EI-EM-EO)
  //comb2 for A and C side (this is CSC-EM-EO)
  //comb3 for A and C side (this is EI-EE-EM)
  //comb4 for A and C side (this is EI-BEE-EM)
  ATH_MSG_INFO ("----------- Combinations are designated as follows: ");
  ATH_MSG_INFO ("----------- Comb 1 is EI-EM-EO ");
  ATH_MSG_INFO ("----------- Comb 2 is CSC-EM-EO ");
  ATH_MSG_INFO ("----------- Comb 3 is EI-EE-EM ");
  ATH_MSG_INFO ("----------- Comb 4 is EI-BEE-EM ");

  CHECK( book( TH1F("hist_nMuons", "Number of muons", 100, 0.0, 100.0) ) );
  CHECK( book( TH1F("hist_nSegments", "Number of muon segments", 200, 0.0, 200.0) ) );

  return StatusCode::SUCCESS;
}

StatusCode MyAnalysisAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //  newTreeFriend->Write();
  return StatusCode::SUCCESS;
}

double MyAnalysisAlg::DeltaR(double phi1, double eta1, double phi2, double eta2)
{
  double deltaR = 0.0;
  double deta = eta1 - eta2;
  double phi_1 = phi1;
  double phi_2 = phi2;
  double dphi = phi_1 - phi_2;

  if (dphi > TMath::Pi()){
    dphi = dphi - TMath::Pi()*2.;
  }
  if (dphi < -1.*TMath::Pi()){
    dphi = TMath::Pi()*2. + dphi;
  }
  deltaR = sqrt(deta*deta + dphi*dphi);
  return deltaR;
}


int MyAnalysisAlg::trkMatchIndex(double tp_phi, double tp_eta, double &valdR) {
  const TrackCollection *sometracks = 0;
  //broken - should probably fix
  //CHECK( evtStore()->retrieve( sometracks, m_TrkTrackCollection ) );
  //i did this instead
  evtStore()->retrieve( sometracks, m_TrkTrackCollection );

  int itr = 0;
  int index = -1;
  double dR = 100.;

  for (const auto* track: *sometracks) {

    const Trk::Perigee* perig = track->perigeeParameters ();
    if (!perig){
      ATH_MSG_ERROR( "-------------- No Trk::Track perigee link " );
      continue;
    }

    double trk_theta = perig->parameters()[Trk::theta];
    double trk_eta    = -log(tan(trk_theta/2)); //asinh(1./tan(trk_theta));
    double trk_phi =  perig->parameters()[Trk::phi];

    double dR_temp = DeltaR(tp_phi, tp_eta, trk_phi, trk_eta);
    if (dR_temp < dR)
      dR = dR_temp;

    if (dR < 0.1){
      index = itr;
      valdR = dR;
    }

    itr++;
  }
  return index;
}

std::string MyAnalysisAlg::ChamberIndexToString (int i ){
  std::string chamberind;
  if (i == 0) chamberind = "BIS";
  if (i == 1) chamberind = "BIL";
  if (i == 2) chamberind = "BMS";
  if (i == 3) chamberind = "BML";
  if (i == 4) chamberind = "BOS";
  if (i == 5) chamberind = "BOL";
  if (i == 6) chamberind = "beE";
  if (i == 7) chamberind = "eiS";
  if (i == 8) chamberind = "eiL";
  if (i == 9) chamberind = "emS";
  if (i == 10) chamberind = "emL";
  if (i == 11) chamberind = "eoS";
  if (i == 12) chamberind = "eoL";
  if (i == 13) chamberind = "eeS";
  if (i == 14) chamberind = "eeL";
  if (i == 15) chamberind = "csS";
  if (i == 16) chamberind = "csL";
  return chamberind;
}

std::string MyAnalysisAlg::hitChamberType (const xAOD::MuonSegment* muonseg){

  std::string chamber;
  chamber = ChamberIndexToString( muonseg->chamberIndex() );
  if (muonseg->etaIndex() > 0 && muonseg->sector() < 10)
    chamber += "A0"+std::to_string(muonseg->sector());
  if (muonseg->etaIndex() > 0 && muonseg->sector() >= 10)
    chamber += "A"+std::to_string(muonseg->sector());
  if (muonseg->etaIndex() <= 0 && muonseg->sector() < 10)
    chamber += "C0"+std::to_string(abs(muonseg->sector()) );
  if (muonseg->etaIndex() <= 0 && muonseg->sector() >= 10)
    chamber += "C"+std::to_string(abs(muonseg->sector()) );

  ATH_MSG_DEBUG( "------------- DEBUG information about segment type ");
  ATH_MSG_DEBUG( "------------- x,y,z position: " << muonseg->x() << ", " << muonseg->y() << ", " << muonseg->z() );
  ATH_MSG_DEBUG( "------------- Selected muon segment: sector = " << muonseg->sector() );
  ATH_MSG_DEBUG( "------------- Selected muon segment: chamber index = " << muonseg->chamberIndex() );
  ATH_MSG_DEBUG( "------------- Selected muon segment: eta index = " << muonseg->etaIndex() );
  ATH_MSG_DEBUG( "------------- Selected muon segment: technology = " << muonseg->technology() );

  return chamber;

}

Identifier  MyAnalysisAlg::getChId( Muon::MuonSegment& seg )
{
  
  //const std::vector<const Trk::RIO_OnTrack*>& rots = seg.containedROTs();
  //if( rots.empty() ) {
  //  return Identifier();
  //}
  //v added

  if(seg.numberOfContainedROTs()<1) return Identifier();

 
  //std::vector<const Trk::RIO_OnTrack*>::const_iterator rit = rots.begin();
  //std::vector<const Trk::RIO_OnTrack*>::const_iterator rit_end = rots.end();
  
  //for( ; rit!=rit_end;++rit ){
  //v added
  for (unsigned int rit = 0; rit < seg.numberOfContainedROTs(); rit++){   
    // use pointer to rot
    const Trk::RIO_OnTrack* rot = seg.rioOnTrack(rit);
    
    if( m_mdtIdHelper->is_mdt( rot->identify() ) ){
      return rot->identify();
    }else if ( m_cscIdHelper->is_csc( rot->identify() ) ){
      return rot->identify();
    }
  }

  // if we get here the segment did not contain any csc or mdt hits, in which case we return the identifier of the first rot
  return seg.rioOnTrack(0)->identify();
}

std::vector<Identifier>  MyAnalysisAlg::getvectChId( Muon::MuonSegment& seg )
{

  std::vector<Identifier> vec;
  //const std::vector<const Trk::RIO_OnTrack*>& rots = seg.containedROTs();
  if( seg.numberOfContainedROTs()< 0 ) {
    vec.push_back(Identifier());
    return vec;
  }

  //  std::vector<const Trk::RIO_OnTrack*>::const_iterator rit = rots.begin();
  //  std::vector<const Trk::RIO_OnTrack*>::const_iterator rit_end = rots.end();


  //for( ; rit!=rit_end;++rit ){
  //v added
  for (unsigned int rit = 0; rit < seg.numberOfContainedROTs(); rit++){   
    // use pointer to rot
    const Trk::RIO_OnTrack* rot = seg.rioOnTrack(rit);

    if( m_mdtIdHelper->is_mdt( rot->identify() ) ){
      vec.push_back(rot->identify());
    }else if( m_cscIdHelper->is_csc( rot->identify() ) ){
      vec.push_back(rot->identify());
    }
    
  }
  return vec;

}



int MyAnalysisAlg::numRoIs( Muon::MuonSegment& seg )
{
  return seg.numberOfContainedROTs();
  //  const std::vector<const Trk::RIO_OnTrack*>& rots = seg.containedROTs();
  // if( rots.empty() ) {
  //  return 0;
  // }

  //  return rots.size();
}


int MyAnalysisAlg::numSegHits( Muon::MuonSegment& seg )
{

  //  const std::vector<const Trk::RIO_OnTrack*>& rots = seg.containedROTs();
  if( seg.numberOfContainedROTs()<1 ) {
    //if( m_debug ) m_log << MSG::DEBUG << " Oops, segment without hits!!! " << endreq;
    return 0;
  }

  // std::vector<const Trk::RIO_OnTrack*>::const_iterator rit = rots.begin();
  // std::vector<const Trk::RIO_OnTrack*>::const_iterator rit_end = rots.end();
  int count = 0;
  for (unsigned int rit = 0; rit < seg.numberOfContainedROTs(); rit++){   
    // use pointer to rot
    const Trk::RIO_OnTrack* rot = seg.rioOnTrack(rit);

    if (rot == 0)
      continue;
    if (rot->identify() !=0)
      count ++;
    
  }
  return count;
}


int MyAnalysisAlg::numSegSegHitShare( Muon::MuonSegment& seg, Muon::MuonSegment &segger )
{
  
  //  const std::vector<const Trk::RIO_OnTrack*>& rots = seg.containedROTs();
  //  const std::vector<const Trk::RIO_OnTrack*>& rotsr = segger.containedROTs();
  if( seg.numberOfContainedROTs() < 1 || segger.numberOfContainedROTs()<1 ) {
    //if( m_debug ) m_log << MSG::DEBUG << " Oops, segment without hits!!! " << endreq;
    return 0;
  }
  
  //  std::vector<const Trk::RIO_OnTrack*>::const_iterator rit = rots.begin();
  // std::vector<const Trk::RIO_OnTrack*>::const_iterator rit_end = rots.end();
  // std::vector<const Trk::RIO_OnTrack*>::const_iterator ritr = rotsr.begin();
  // std::vector<const Trk::RIO_OnTrack*>::const_iterator ritr_end = rotsr.end();
  int count = 0;
  //for( ; rit!=rit_end;++rit ){
  for (unsigned int rit = 0; rit < seg.numberOfContainedROTs(); rit++){   
    // use pointer to rot
    const Trk::RIO_OnTrack* rot = seg.rioOnTrack(rit);
    //    const Trk::RIO_OnTrack* rot = *rit;
    if (rot == 0 || rot->identify() == 0)
      continue;
    for (unsigned int ritr = 0; ritr < segger.numberOfContainedROTs(); ritr++){
      //    for( ; ritr!=ritr_end;++ritr ){
      const Trk::RIO_OnTrack* rotr = segger.rioOnTrack(ritr);
      if (rotr == 0 || rotr->identify() == 0)
	continue;
      
      if (rot->identify() == rotr->identify())
	count ++;
    }
  }
  return count;
}




int MyAnalysisAlg::numTrackHits(const Trk::Track* trk)
{
  const DataVector<const Trk::MeasurementBase>* trk_mT = trk->measurementsOnTrack();
  int count = 0;
  ATH_MSG_DEBUG("In numTrackHits");
  for (int i = 0; i < trk_mT->size(); i++){
    if (trk_mT->at(i) == 0)
      continue;

    const Trk::RIO_OnTrack* rio = dynamic_cast<const Trk::RIO_OnTrack*>(trk_mT->at(i));
    if (rio == 0)
      continue;

    if (rio->identify() != 0){
      count++;
    }

  }
  return count;
}


int MyAnalysisAlg::segtrackHitsOverlap( const Trk::Segment* seg1, const Trk::Track* trk)
{

  Muon::MuonSegment *seg = (Muon::MuonSegment*) seg1; //Muon::MuonSegment(seg1);
  //  const Muon::MuonSegment *seg = dynamic_cast<const Muon::MuonSegment*>(seg1);
  //  const std::vector<const Trk::RIO_OnTrack*>& rots = seg->containedROTs();
  std::vector<const Trk::RIO_OnTrack*> trots;
  trots.clear();
  
  const DataVector<const Trk::MeasurementBase>* trk_mT = trk->measurementsOnTrack();
  for (int i = 0; i < trk_mT->size(); i++){
    
    const Trk::RIO_OnTrack* rio = dynamic_cast<const Trk::RIO_OnTrack*>(trk_mT->at(i));
    trots.push_back(rio);
  }
  
  if( seg->numberOfContainedROTs()<1 || trots.empty()) {
    //if( m_debug ) m_log << MSG::DEBUG << " Oops, segment without hits!!! " << endreq;
    return 0;
  }


  std::vector<bool> vcount;
  for (int i = 0; i < int(seg->numberOfContainedROTs()); i++){
    vcount.push_back(false);
    const Trk::RIO_OnTrack *rot = seg->rioOnTrack(i);
    for (int j = 0; j < int(trots.size()); j++){
      const Trk::RIO_OnTrack *trot = (trots[j]);
      if (trot != 0 && rot->identify() == trot->identify())
	vcount[i] = true;
    }
  }

  int count = 0;
  for (int i = 0; i < vcount.size(); i++)
    if (vcount[i])
      count++;
  return count;
  
}

Amg::Transform3D MyAnalysisAlg::getGlobalToStation( Identifier& id )
{
  if( m_mdtIdHelper->is_mdt( id ) ){
    const MuonGM::MdtReadoutElement* detEl = m_detMgr->getMdtReadoutElement(id);
    if( !detEl ) {
      ATH_MSG_ERROR("getGlobalToStation failed to retrieve detEL");
    }else{
        return detEl->GlobalToAmdbLRSTransform();
  
    }
  }else if( m_cscIdHelper->is_csc( id ) ){
    const MuonGM::CscReadoutElement* detEl = m_detMgr->getCscReadoutElement(id);
    if( !detEl ) {
      ATH_MSG_ERROR("getGlobalToStation failed to retrieve detEL");      //m_log << MSG::WARNING << "getGlobalToStation failed to retrieve detEL byebye" << endreq;
    }else{
      return detEl->transform().inverse();
    }
  }else if( m_tgcIdHelper->is_tgc( id ) ){
    const MuonGM::TgcReadoutElement* detEl = m_detMgr->getTgcReadoutElement(id);
    if( !detEl ) {
      ATH_MSG_ERROR("getGlobalToStation failed to retrieve detEL");      //m_log << MSG::WARNING << "getGlobalToStation failed to retrieve detEL byebye" << endreq;
    }else{
      return detEl->transform().inverse();
    }
  }else if( m_rpcIdHelper->is_rpc( id ) ){
    const MuonGM::RpcReadoutElement* detEl = m_detMgr->getRpcReadoutElement(id);
    if( !detEl ) {
      ATH_MSG_ERROR("getGlobalToStation failed to retrieve detEL");      //m_log << MSG::WARNING << "getGlobalToStation failed to retrieve detEL byebye" << endreq;
    }else{
      return detEl->transform().inverse();
    }
  }else{
    return Amg::Transform3D();
  }
  return Amg::Transform3D();
}


// -------------------------------------------------------
// ------------- Functions (some a bit redundant)
// ------------- to perform cuts on segment selection
// -------------------------------------------------------

// ------------- Restrict number of segments per sector
bool MyAnalysisAlg::CUT_segsPerSector(int count)
{
  bool pass = false;
  if (count >= m_minSegPerSector && count <= m_maxSegPerSector)
    pass = true;
  return pass;
}

bool MyAnalysisAlg::CUT_hitsOnSegment (const xAOD::MuonSegment* muonseg){
  std::string name = hitChamberType(muonseg);
  std::string nshort = name.std::string::substr(0,2);
  bool pass = false;

  //Only one of following conditions will apply (dependent on chamber type)
  if (nshort == "ei" && muonseg->nPrecisionHits() >= m_minHitsEI) pass = true;
  if (nshort == "ee") pass = true;
  if (nshort == "em" && muonseg->nPrecisionHits() >= m_minHitsEM) pass = true;
  if (nshort == "eo" && muonseg->nPrecisionHits() >= m_minHitsEO) pass = true;
  if (nshort == "cs") pass = true;
  if (nshort == "be" && muonseg->nPrecisionHits() >= m_minHitsBEE && muonseg->nPrecisionHits() <= m_maxHitsBEE) pass = true;

  return pass;

}

// ------------- Restrict fit quality of segment under consideration
bool MyAnalysisAlg::CUT_fitqualityOfSegment (const xAOD::MuonSegment* muonseg)
{
  bool pass = false;
  if (muonseg->chiSquared()/muonseg->numberDoF() < m_fitQualPerSeg)
    pass = true;
  return pass;
}

//bool BaseAnalysis::CUT_edgeOfChamber (){}
//bool BaseAnalysis::CUT_hitsInNeigbourChamber (){}

//Functions to perform selection between segment(s) and tracks
//In addition to below, are already looping over segments per sector (so sector restriction already implicit)
bool MyAnalysisAlg::CUTS_matchSegTrack(const Trk::Segment *segment, const Trk::Track *track) {
  bool pass = true;
  const std::vector<const Trk::MeasurementBase*> seg_vMB = segment->containedMeasurements();
  const DataVector<const Trk::MeasurementBase>* track_vMB = track->measurementsOnTrack();

  std::vector<bool> v_match;

  for (size_t i = 0; i < seg_vMB.size(); i++){
    const Trk::MeasurementBase* segMB = seg_vMB[i];
    const Amg::Vector3D& segMB_GP = segMB->globalPosition();
    double segX = segMB_GP.x();
    double segY = segMB_GP.y();
    double segZ = segMB_GP.z();

    v_match.push_back(false);
    for (size_t j = 0; j < track_vMB->size(); j++){
      const Trk::MeasurementBase *trackMB = track_vMB->at(j);
      const Amg::Vector3D& trackMB_GP = trackMB->globalPosition();
      double trackX = trackMB_GP.x();
      double trackY = trackMB_GP.y();
      double trackZ = trackMB_GP.z();

      if (segX == trackX && segY == trackY && segZ == trackZ)
	v_match[i] = true;
      
    }//end loop over track measurments
  }//end loop over segment measurments
  
  for (size_t i = 0; i < v_match.size(); i++)
    if (!v_match[i])
      pass = false;

  return pass;
}

//bool BaseAnalysis::CUTS_authorSegs() {}
//bool BaseAnalysis::CUTS_TGCPhiHit() {}
//bool BaseAnalysis::CUTS_pointingSegTrack () {}
//bool BaseAnalysis::CUTS_pointingSegs () {}

// -------------------------------------------------------
// ------------- execute() method
// -------------------------------------------------------

StatusCode MyAnalysisAlg::execute() {

  
  ATH_MSG_DEBUG( "---------------------- Retrieve the Trk::SegmentCollection" );
  //Now work through the valid combinations (per sector)

  // Retrieve the Trk::SegmentCollection:
  const Trk::SegmentCollection* segs = 0; // a const here as we will not modify this container
  CHECK( evtStore()->retrieve( segs, "TrackMuonSegments" ) );

  ATH_MSG_DEBUG( "---------------------- Retrieve the TrackCollection" );
  const TrackCollection *tracks = 0;
  CHECK( evtStore()->retrieve( tracks, "MuonSpectrometerTracks" ) );

  ATH_MSG_DEBUG ("Executing " << name() << "...");
  const xAOD::EventInfo* eventInfo = 0;
  CHECK( evtStore()->retrieve( eventInfo) );

  // Retrieve the muons:
  const xAOD::MuonContainer* muons = 0; // a const here as we will not modify this container
  CHECK( evtStore()->retrieve( muons, "Muons" ) );
  hist("hist_nMuons")->Fill( muons->size() );
  nMuons = muons->size();
  eRunNumber = eventInfo->runNumber();
  eEventNumber = eventInfo->eventNumber();

  eLumiBlock = eventInfo->lumiBlock();


  enMuons = muons->size();


  const xAOD::MuonSegmentContainer* muonsegs_temp = 0; // a const here as we will not modify this container
  CHECK( evtStore()->retrieve( muonsegs_temp, "MuonSegments" ) );
  int segger_size = muonsegs_temp->size();
  std::vector<int> segger_author;
  segger_author.resize(segger_size, -999999);
  std::vector<int> segger_muon_author;
  segger_muon_author.resize(segger_size, -999999);
  std::vector<bool> segger_hitsOnTrack;
  segger_hitsOnTrack.resize(segger_size, -999999);

  std::vector<int> segger_muon_index;
  segger_muon_index.resize(segger_size, -999999.);
  std::vector<double> segger_muon_x;
  segger_muon_x.resize(segger_size, -999999.);
  std::vector<double> segger_muon_y;
  segger_muon_y.resize(segger_size, -999999.);
  std::vector<double> segger_muon_z;
  segger_muon_z.resize(segger_size, -999999.);  

  std::vector<double> segger_muon_px;
  segger_muon_px.resize(segger_size, -999999.);
  std::vector<double> segger_muon_py;
  segger_muon_py.resize(segger_size, -999999.);
  std::vector<double> segger_muon_pz;
  segger_muon_pz.resize(segger_size, -999999.);

  std::vector<int> segger_track_index;
  segger_track_index.resize(segger_size, -999999.);

  std::vector<double> segger_track_x;
  segger_track_x.resize(segger_size, -999999.);
  std::vector<double> segger_track_y;
  segger_track_y.resize(segger_size, -999999.);
  std::vector<double> segger_track_z;
  segger_track_z.resize(segger_size, -999999.);  

  std::vector<double> segger_track_px;
  segger_track_px.resize(segger_size, -999999.);
  std::vector<double> segger_track_py;
  segger_track_py.resize(segger_size, -999999.);
  std::vector<double> segger_track_pz;
  segger_track_pz.resize(segger_size, -999999.);

  std::vector<double> segger_muon_pt;
  segger_muon_pt.resize(segger_size, -999999.);
  std::vector<double> segger_muon_eta;
  segger_muon_eta.resize(segger_size, -999999.);
  std::vector<double> segger_muon_phi;
  segger_muon_phi.resize(segger_size, -999999.);


  std::vector<int> segger_nhits;
  segger_nhits.resize(segger_size, -999999.);
  std::vector<int> segger_ntrackhits;
  segger_ntrackhits.resize(segger_size, -999999.);
  std::vector<int> segger_track_hitshare;
  segger_track_hitshare.resize(segger_size, -999999.);
  std::vector< std::vector<int> > segger_hitshare;
  segger_hitshare.resize(segger_size);//, -999999.);

  
  std::vector<double> segger_hitavg_global_x;
  segger_hitavg_global_x.resize(segger_size, -999999.);
  std::vector<double> segger_hitavg_global_y;
  segger_hitavg_global_y.resize(segger_size, -999999.);
  std::vector<double> segger_hitavg_global_z;
  segger_hitavg_global_z.resize(segger_size, -999999.);

  std::vector<double> segger_hitavg_global_px;
  segger_hitavg_global_px.resize(segger_size, -999999.);
  std::vector<double> segger_hitavg_global_py;
  segger_hitavg_global_py.resize(segger_size, -999999.);
  std::vector<double> segger_hitavg_global_pz;
  segger_hitavg_global_pz.resize(segger_size, -999999.);

  std::vector<double> segger_hitavg_local_x;
  segger_hitavg_local_x.resize(segger_size, -999999.);
  std::vector<double> segger_hitavg_local_y;
  segger_hitavg_local_y.resize(segger_size, -999999.);
  std::vector<double> segger_hitavg_local_z;
  segger_hitavg_local_z.resize(segger_size, -999999.);

  std::vector<double> segger_hitavg_local_px;
  segger_hitavg_local_px.resize(segger_size, -999999.);
  std::vector<double> segger_hitavg_local_py;
  segger_hitavg_local_py.resize(segger_size, -999999.);
  std::vector<double> segger_hitavg_local_pz;
  segger_hitavg_local_pz.resize(segger_size, -999999.);
  
  std::vector<double> segger_trk_match_id_count;
  segger_trk_match_id_count.resize(segger_size, -999999.);
  // ------------------------------------------------------------------------------------
  // --------- Counters for number of muon segments per sector (both A and C side)
  // ------------------------------------------------------------------------------------
  int countSegInSecXAside[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  int countSegInSecXCside[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  //------------- vector pair to store segment name and index (both A and C side and each sector)
  std::vector<std::pair<std::string,int>> EI_Aside_seg[16] = {};
  std::vector<std::pair<std::string,int>> EE_Aside_seg[16] = {};
  std::vector<std::pair<std::string,int>> EM_Aside_seg[16] = {};
  std::vector<std::pair<std::string,int>> EO_Aside_seg[16] = {};
  std::vector<std::pair<std::string,int>> CSC_Aside_seg[16] = {};
  std::vector<std::pair<std::string,int>> BEE_Aside_seg[16] = {};
  
  std::vector<std::pair<std::string,int>> EI_Cside_seg[16] = {};
  std::vector<std::pair<std::string,int>> EE_Cside_seg[16] = {};
  std::vector<std::pair<std::string,int>> EM_Cside_seg[16] = {};
  std::vector<std::pair<std::string,int>> EO_Cside_seg[16] = {};
  std::vector<std::pair<std::string,int>> CSC_Cside_seg[16] = {};
  std::vector<std::pair<std::string,int>> BEE_Cside_seg[16] = {};

  muon_n = 0;
  for( const auto* muon : *muons ) {
    muon_n++;
    // -------------------------------------------------------
    // ------- check whether there is an associated track
    // -------------------------------------------------------
    //    const xAOD::TrackParticle* muontp = muon->trackParticle(m_TrackParticleCollection);
    int muonTrmatch = -1;
    // const xAOD::TrackParticle* muontp = muon->trackParticle(xAOD::Muon::CombinedTrackParticle);//);MuonSpectrometerTrackParticle
    const xAOD::TrackParticle* muontp;
    if (muon->muonSpectrometerTrackParticleLink().isValid() && muon->combinedTrackParticleLink().isValid()) {
      muontp = muon->trackParticle(muon->MuonSpectrometerTrackParticle);
    }

    if (!muontp) {
      ATH_MSG_INFO( "------------- No muon track particle " );
      continue; // non existent for e.g. segment tagged muons
    }


    //ATH_MSG_INFO("Muon TP author: " << muontp->author() );

    const Trk::Track* muontr = muontp->track();
    muonTrmatch = 1;

    if (!muontr) { // at this point, this should always exist
      ATH_MSG_INFO("Track particle points to null Trk::Track pointer, trying to match with DeltaR");
      const TrackCollection *tracks = 0;
      CHECK( evtStore()->retrieve( tracks, "CombinedMuonTracks" ) ); //MuonSpectrometerTracks
      double tp_phi = muontp->phi();
      double tp_eta = muontp->eta();
      double dR_match;
      int track_index = trkMatchIndex(tp_phi, tp_eta, dR_match);
      if (track_index == -1){
	      ATH_MSG_INFO( "------------- No muon trk::track " );
	      muonTrmatch = 0;
	      //continue;
      } else{
	      muontr = tracks->at(track_index);
	      muonTrmatch = 2;
      }
    }

    if (muontr != 0)
      ATH_MSG_INFO("Muon TR author: " << muontr->info() );


    mRunNumber = eventInfo->runNumber();
    mEventNumber = eventInfo->eventNumber();
    muon_pt = muon->pt();
    muon_eta = muon->eta();
    muon_phi = muon->phi();
    muon_m = muon->m();
    muon_e = muon->e();
    muon_charge = muon->charge();
    muon_type = muon->muonType();
    muon_EnergyLossType = muon->energyLossType();    
    muon_author = muon->author();
    muon_quality = muon->quality();
    muon_pass_IDhits = muon->passesIDCuts();
    muon_pass_HighPt = muon->passesHighPtCuts();
    muon_nSegments = muon->nMuonSegments();
    muon_trMatch = muonTrmatch;
    muon_allAuthors = muon->allAuthors();

    muon_numberOfPrecisionLayers = muon->auxdata< uint8_t >( "numberOfPrecisionLayers" );
    muon_numberOfPrecisionHoleLayers = muon->auxdata< uint8_t >( "numberOfPrecisionHoleLayers" );
    muon_numberOfPhiLayers =  muon->auxdata< uint8_t >( "numberOfPhiLayers" );
    muon_numberOfPhiHoleLayers = muon->auxdata< uint8_t >( "numberOfPhiHoleLayers" );
    muon_numberOfTriggerEtaLayers = muon->auxdata< uint8_t >( "numberOfTriggerEtaLayers" );
    muon_numberOfTriggerEtaHoleLayers = muon->auxdata< uint8_t >( "numberOfTriggerEtaHoleLayers" );
    muon_primarySector = muon->auxdata< uint8_t >( "primarySector" );
    muon_secondarySector = muon->auxdata< uint8_t >( "secondarySector" );
    muon_innerSmallHits = muon->auxdata< uint8_t >( "innerSmallHits" );
    muon_innerLargeHits = muon->auxdata< uint8_t >( "innerLargeHits" );
    muon_middleSmallHits =  muon->auxdata< uint8_t >( "middleSmallHits" );
    muon_middleLargeHits = muon->auxdata< uint8_t >( "middleLargeHits" );
    muon_outerSmallHits = muon->auxdata< uint8_t >( "outerSmallHits" );
    muon_outerLargeHits = muon->auxdata< uint8_t >( "outerLargeHits" );
    muon_extendedSmallHits = muon->auxdata< uint8_t >( "extendedSmallHits" );
    muon_extendedLargeHits = muon->auxdata< uint8_t >( "extendedLargeHits" );
    muon_innerSmallHoles = muon->auxdata< uint8_t >( "innerSmallHoles" );
    muon_innerLargeHoles = muon->auxdata< uint8_t >( "innerLargeHoles" );
    muon_middleSmallHoles = muon->auxdata< uint8_t >( "middleSmallHoles" );
    muon_middleLargeHoles = muon->auxdata< uint8_t >( "middleLargeHoles" );
    muon_outerSmallHoles = muon->auxdata< uint8_t >( "outerSmallHoles" );
    muon_outerLargeHoles = muon->auxdata< uint8_t >( "outerLargeHoles" );
    muon_extendedSmallHoles = muon->auxdata< uint8_t >( "extendedSmallHoles" );
    muon_extendedLargeHoles = muon->auxdata< uint8_t >( "extendedLargeHoles" );
 
    isAuthorUnknown = muon->isAuthor(xAOD::Muon::Author::unknown);
    isAuthorMuid = muon->isAuthor(xAOD::Muon::Author::MuidCo);
    isAuthorStaco = muon->isAuthor(xAOD::Muon::Author::STACO);
    isAuthorMuTag = muon->isAuthor(xAOD::Muon::Author::MuTag);
    isAuthorMuTagIMO = muon->isAuthor(xAOD::Muon::Author::MuTagIMO);
    isAuthorMuidSA = muon->isAuthor(xAOD::Muon::Author::MuidSA);
    isAuthorMuGirl = muon->isAuthor(xAOD::Muon::Author::MuGirl);
    isAuthorMuGirlLowBeta = muon->isAuthor(xAOD::Muon::Author::MuGirlLowBeta);
    isAuthorCaloTag = muon->isAuthor(xAOD::Muon::Author::CaloTag);
    isAuthorCaloLikelihood = muon->isAuthor(xAOD::Muon::Author::CaloLikelihood);
    isAuthorExtrapolateMuonToIP = muon->isAuthor(xAOD::Muon::Author::ExtrapolateMuonToIP);
    
    
    mspectrometerFieldIntegral=0.; //<! B-field integral in MS
    mscatteringCurvatureSignificance=0.; //<! Scattering angle significance: curvature
    mscatteringNeighbourSignificance=0.; //<! Scattering angle significance: neighbours
    mmomentumBalanceSignificance=0.; //<! momentum balance significance
   
    
    muon->parameter(mspectrometerFieldIntegral,xAOD::Muon::ParamDef::spectrometerFieldIntegral);
    muon->parameter(mscatteringCurvatureSignificance,xAOD::Muon::ParamDef::scatteringCurvatureSignificance);
    muon->parameter(mscatteringNeighbourSignificance, xAOD::Muon::ParamDef::scatteringNeighbourSignificance);
    muon->parameter(mmomentumBalanceSignificance, xAOD::Muon::ParamDef::momentumBalanceSignificance);

    mLumiBlock = eventInfo->lumiBlock();
 
    mtree->Fill();
  
    // Find the segments associated to this muon
    std::vector< ElementLink<xAOD::MuonSegmentContainer> > cb_mu_seg_links = muon->muonSegmentLinks();
      // muon->auxdata< std::vector< ElementLink<xAOD::MuonSegmentContainer> > >("segmentsOnTrack");
    
    // Start looping through all the segments associated to this muon
    // Note, I think "muon->nMuonSegments" gets information from xAOD containers, rather than ESD ones and shouldn't be used 
    for (size_t s = 0; s != cb_mu_seg_links.size(); s++){
          
      if( ! cb_mu_seg_links[s].isValid() ) {
	      ATH_MSG_INFO( "ERROR -- missing link -- should not happen" );
	      continue;
      }
      
      seg_index = -1; //initialize to invalid value, will set to correct index below
      const xAOD::MuonSegment *museg = *cb_mu_seg_links[s]; //muon->muonSegment(s);

      // Go from muonSegment to the TrkSegment 
      const ElementLink<DataVector<Trk::Segment> > museg_link = museg->muonSegment();
      if (!museg_link.isValid()) {
        ATH_MSG_DEBUG( "ElementLink from xAOD::MuonSegment to Trk::Segement not valid" );
        continue;
      }
      const Trk::Segment *seg = segs->at(museg_link.persIndex());
      seg_index = cb_mu_seg_links[s].persIndex(); // corresponding index in segment containers;

      Muon::MuonSegment *mseg = (Muon::MuonSegment*) seg; //can cast TrkSgment to ESD MuonSegment
      
      int n_Segs = numSegHits( *mseg );
      int n_TrackHits = numTrackHits( muontr );
      int n_Seg_Track_OvLp = segtrackHitsOverlap(seg, muontr);
      
      const Trk::LocalDirection& locdir_mseg = mseg->localDirection();
      const Trk::PlaneSurface& psurf_mseg = mseg->associatedSurface();
      const Amg::Vector3D& ideal_loc = psurf_mseg.globalReferencePoint();
      
      Identifier chid = getChId( *mseg );
      std::vector<Identifier> seg_chid = getvectChId( *mseg );
      Amg::Transform3D gToStationCheck = (*mseg).associatedSurface().transform().inverse();
      Amg::Transform3D gToStation = getGlobalToStation( chid );
      
      // create the local position and direction vector
      Amg::Vector3D  segPosG((*mseg).globalPosition());
      Amg::Vector3D segDirG((*mseg).globalDirection());
      
      // calculate local position and direction of segment
      Amg::Vector3D segPosL  = gToStation*segPosG;
      Amg::Vector3D segPosLCheck  = gToStationCheck*segPosG;
      Amg::Vector3D segDirL = gToStation.linear()*segDirG;
      Amg::Vector3D segDirLCheck = gToStationCheck.linear()*segDirG;

      const DataVector<const Trk::MeasurementBase>* trk_mT = muontr->measurementsOnTrack();
      const DataVector<const Trk::TrackStateOnSurface>* trk_s = muontr->trackStateOnSurfaces();
      
      // arrays to store calculated average readings (this is for track based second coord)
      double trk_avg_pos_global[3] = {0.,0.,0.};
      double trk_avg_dir_global[3] = {0.,0.,0.};
      double trk_avg_pos_local[3] = {0.,0.,0.};
      double trk_avg_dir_local[3] = {0.,0.,0.};
      double count = 0.;

      for (int i = 0; i < trk_s->size(); i++){
	if (trk_s->at(i) == 0)
	  continue;
	const Trk::TrackParameters *trk_par = trk_s->at(i)->trackParameters();
	const Trk::MeasurementBase *trk_mb = trk_s->at(i)->measurementOnTrack();
	if (trk_par == 0)
	  continue;
	const Trk::RIO_OnTrack* rio = dynamic_cast<const Trk::RIO_OnTrack*>(trk_mb);
	if (rio == 0)
	  continue;
	if (rio->identify() != 0){
	  for (int id_ind = 0; id_ind < seg_chid.size(); id_ind++){
	    Identifier this_id = seg_chid[id_ind];
	    if (rio->identify() == this_id){
	      const Trk::Surface &psurf_track = trk_mb->associatedSurface();
	      if (count != 0){
		const Trk::Surface &psurf_track_oth = trk_mb->associatedSurface();
		if (psurf_track != psurf_track_oth)
		  continue;	      
	      }
	      Amg::Transform3D g = psurf_track.transform().inverse();
	      Amg::Vector3D s = g*segPosG;
	      Amg::Vector3D sd = g.linear()*segDirG;

	      Amg::Vector3D tp = trk_par->position();
	      double tphi = trk_par->parameters()[Trk::phi0];
	      double ttheta = trk_par->parameters()[Trk::theta];
	      Amg::Vector3D td = Amg::Vector3D(cos(tphi)*sin(ttheta), sin(tphi)*sin(ttheta), cos(ttheta) );

	      Amg::Vector3D SegPosLL  = gToStation*tp;	
	      Amg::Vector3D SegDirLL =  gToStation.linear()*td;

	      trk_avg_pos_global[0] += s.x(); 	      trk_avg_pos_global[1] += s.y(); 	      trk_avg_pos_global[2] += s.z();
	      trk_avg_dir_global[0] += sd.x();	      trk_avg_dir_global[1] += sd.y();	      trk_avg_dir_global[2] += sd.z();
	      trk_avg_pos_local[0] += SegPosLL.x();       trk_avg_pos_local[1] += SegPosLL.y();       trk_avg_pos_local[2] += SegPosLL.z();
	      trk_avg_dir_local[0] += SegDirLL.x();      trk_avg_dir_local[1] += SegDirLL.y();      trk_avg_dir_local[2] += SegDirLL.z();
	      count += 1.;
	    }//id = chid
	  }//loop over chid
	}//if rio
      }//trk surface loop
      
      seg_trk_match_id = count;
      
      if (count != 0){
	trk_avg_pos_global[0] = trk_avg_pos_global[0]/count;      trk_avg_pos_global[1] = trk_avg_pos_global[1]/count;      trk_avg_pos_global[2] = trk_avg_pos_global[2]/count;
	trk_avg_dir_global[0] = trk_avg_dir_global[0]/count;      trk_avg_dir_global[1] = trk_avg_dir_global[1]/count;      trk_avg_dir_global[2] = trk_avg_dir_global[2]/count;
	trk_avg_pos_local[0]  = trk_avg_pos_local[0]/count;       trk_avg_pos_local[1]  = trk_avg_pos_local[1]/count;       trk_avg_pos_local[2]  = trk_avg_pos_local[2]/count;
	trk_avg_dir_local[0]  = trk_avg_dir_local[0]/count;       trk_avg_dir_local[1]  = trk_avg_dir_local[1]/count;       trk_avg_dir_local[2]  = trk_avg_dir_local[2]/count;
      }
      
      // Set values that will be stored to ntuple
      seg_trk_pos_global_x = trk_avg_pos_global[0];      seg_trk_pos_global_y = trk_avg_pos_global[1];      seg_trk_pos_global_z = trk_avg_pos_global[2];
      seg_trk_pos_local_x  = trk_avg_pos_local[0];       seg_trk_pos_local_y  = trk_avg_pos_local[1];       seg_trk_pos_local_z  = trk_avg_pos_local[2];
      seg_trk_dir_global_x = trk_avg_dir_global[0];      seg_trk_dir_global_y = trk_avg_dir_global[1];      seg_trk_dir_global_z = trk_avg_dir_global[2];
      seg_trk_dir_local_x  = trk_avg_dir_local[0];       seg_trk_dir_local_y  = trk_avg_dir_local[1];       seg_trk_dir_local_z  = trk_avg_dir_local[2];
      
      // Grab measurement base for this segment and store properties
      const std::vector<const Trk::MeasurementBase*> seg_vMB = seg->containedMeasurements();
      std::vector<bool> v_match;
      seg_vMB_x.clear();
      seg_vMB_y.clear();
      seg_vMB_z.clear();
      std::vector<int> seg_hit_count;
      
      for (size_t i = 0; i < seg_vMB.size(); i++){
	int currentHits = seg_vMB.size();
	const Trk::MeasurementBase* segMB = seg_vMB[i];
	const Amg::Vector3D& segMB_GP = segMB->globalPosition();
	seg_vMB_x.push_back(segMB_GP.x());
	seg_vMB_y.push_back(segMB_GP.y());
	seg_vMB_z.push_back(segMB_GP.z());
	v_match.push_back(false);
      }
      
      //int BaseAnalysis::numSegSegHitShare( Muon::MuonSegment& seg, Muon::MuonSegment &segger )
      const xAOD::MuonSegmentContainer* muonsegs_t = 0;
      CHECK( evtStore()->retrieve( muonsegs_t, "MuonSegments" ) );
      int zcount = 0;
      for (size_t segitr_t = 0; segitr_t < muonsegs_t->size(); segitr_t++){
	const xAOD::MuonSegment *muonseg_t = muonsegs_t->at(segitr_t);
	const ElementLink<DataVector<Trk::Segment> > muonseg_t_link = muonseg_t->muonSegment();
	if (!muonseg_t_link.isValid())
	  ATH_MSG_DEBUG( "ElementLink from xAOD::MuonSegment to Trk::Segement not valid" );
	const Trk::Segment *seg_t = segs->at(muonseg_t_link.persIndex());
	Muon::MuonSegment *msegr = (Muon::MuonSegment*) seg_t; //Muon::MuonSegment(seg1);
	
	zcount = numSegSegHitShare(*mseg, *msegr);
	seg_hit_count.push_back(zcount);
      }
      
      // Consider number of hits that are shared with other segments
      for (size_t i = 0; i < v_match.size(); i++){
	int valid = 0;
	int currentHits = 0; //seg_vMB.size();
	const Trk::MeasurementBase* segMB = seg_vMB[i];
	const Amg::Vector3D& segMB_GP = segMB->globalPosition();
	
	ATH_MSG_DEBUG( "---------------------- Retrieve muon segments" );
	const xAOD::MuonSegmentContainer* muonsegs_t = 0; 
	CHECK( evtStore()->retrieve( muonsegs_t, "MuonSegments" ) );
	ATH_MSG_DEBUG( "---------------------- Starting loop over segments" );
	for (size_t segitr_t = 0; segitr_t < muonsegs_t->size(); segitr_t++){
	  //for( const auto* muonseg : *muonsegs ) {
	  const xAOD::MuonSegment *muonseg_t = muonsegs_t->at(segitr_t);
	  const ElementLink<DataVector<Trk::Segment> > muonseg_t_link = muonseg_t->muonSegment();
	  if (!muonseg_t_link.isValid())
	    ATH_MSG_DEBUG( "ElementLink from xAOD::MuonSegment to Trk::Segement not valid" );
	  const Trk::Segment *seg_t = segs->at(muonseg_t_link.persIndex());
	  const std::vector<const Trk::MeasurementBase*> seg_t_vMB = seg_t->containedMeasurements();
	  //Don't care about matches between same segment
	  if (seg_vMB == seg_t_vMB)
	    continue;

	  for (size_t j = 0; j < seg_t_vMB.size(); j++){
	    const Trk::MeasurementBase* segMB_t = seg_t_vMB[j];
	    const Amg::Vector3D& segMB_t_GP = segMB_t->globalPosition();
	    
	    if (segMB->globalPosition() == segMB_t->globalPosition()){
	      valid++;
	    }
	  }//End secondary measurement loop
	}//End secondary segment loop
	if (valid > currentHits) //Need value other than self match
	  v_match[i] = true;
      }//End first measurment loop
      
      
      bool anysharedhits = false;
      for (int b = 0; b < v_match.size(); b++){
	if (v_match[b] == true)
	  anysharedhits = true;
      }
      

      // Storing information to ntuple
      seg_nhits = n_Segs;
      seg_ntrackhits = n_TrackHits;
      seg_track_hitshare = n_Seg_Track_OvLp;
      seg_hitshare = seg_hit_count; //anysharedhits;
      sRunNumber = eventInfo->runNumber();
      sEventNumber = eventInfo->eventNumber();
      
      seg_muon_n = muon_n;
      seg_author = seg->author();
      seg_eta_index = museg->etaIndex();
      seg_tech = museg->technology();
      seg_cham_index = museg->chamberIndex();
      seg_name = hitChamberType(museg);
      seg_name_short = seg_name.std::string::substr(0,2);;
      seg_haveTrkSeg = muonTrmatch;      
      //      seg_ientifier = chid;
      seg_x = museg->x();
      seg_y = museg->y();
      seg_z = museg->z();
      seg_px = museg->px();
      seg_py = museg->py();
      seg_pz = museg->pz();
      seg_t0 = museg->t0();
      seg_t0_error = museg->t0error();
      seg_chisquare = museg->chiSquared();
      seg_nDOF = museg->numberDoF();
      seg_nPrecisionHits = museg->nPrecisionHits();
      seg_nTrigEtaLayers = museg->nTrigEtaLayers();
      seg_nTrigPhiLayers = museg->nPhiLayers();
      seg_global_x = segPosG.x();
      seg_global_y = segPosG.y();
      seg_global_z = segPosG.z();
      seg_global_px = segDirG.x();
      seg_global_py = segDirG.y();
      seg_global_pz = segDirG.z();
      seg_local_x = segPosL.x();
      seg_local_y = segPosL.y();
      seg_local_z = segPosL.z();
      seg_local_px = segDirL.x();
      seg_local_py = segDirL.y();
      seg_local_pz = segDirL.z();
      seg_cham_x = ideal_loc.x();
      seg_cham_y = ideal_loc.y();
      seg_cham_z = ideal_loc.z();
      seg_hitreq = (CUT_hitsOnSegment (museg)); //check that have valid number of hits for this chamber type
      seg_fitqual = (CUT_fitqualityOfSegment (museg)); //check that the fit quality of the segment is adequate
      
      if (seg_index != -1){
	//	segger_muon_index[seg_index]
	segger_author[seg_index] = seg->author();
	segger_muon_author[seg_index] = muon->author();
	segger_hitsOnTrack[seg_index] = -1;
	
	segger_muon_x[seg_index] = -1;
	segger_muon_y[seg_index] = -1;
	segger_muon_z[seg_index] = -1;
	
	segger_muon_px[seg_index] = -1;
	segger_muon_py[seg_index] = -1;
	segger_muon_pz[seg_index] = -1;
	
	segger_track_x[seg_index] = -1;
	segger_track_y[seg_index] = -1;
	segger_track_z[seg_index] = -1;
	
	segger_track_px[seg_index] = -1;
	segger_track_py[seg_index] = -1;
	segger_track_pz[seg_index] = -1;
	
	segger_muon_pt[seg_index] = muon->pt();
	segger_muon_eta[seg_index] = muon->eta();
	segger_muon_phi[seg_index] = muon->phi();
	
	segger_nhits[seg_index] = n_Segs;
	segger_ntrackhits[seg_index] = n_TrackHits;
	segger_track_hitshare[seg_index] = n_Seg_Track_OvLp;
	//	segger_hitshare[seg_index] = seg_hit_count;
	segger_trk_match_id_count[seg_index] = seg_trk_match_id;
	segger_hitavg_global_x[seg_index] = trk_avg_pos_global[0];
	segger_hitavg_global_y[seg_index] = trk_avg_pos_global[1];
	segger_hitavg_global_z[seg_index] = trk_avg_pos_global[2];
	segger_hitavg_local_x[seg_index] = trk_avg_pos_local[0];
	segger_hitavg_local_y[seg_index] = trk_avg_pos_local[1];
	segger_hitavg_local_z[seg_index] = trk_avg_pos_local[2];
	segger_hitavg_global_px[seg_index] = trk_avg_dir_global[0];
	segger_hitavg_global_py[seg_index] = trk_avg_dir_global[1];
	segger_hitavg_global_pz[seg_index] = trk_avg_dir_global[2];
	segger_hitavg_local_px[seg_index] = trk_avg_dir_local[0];
	segger_hitavg_local_py[seg_index] = trk_avg_dir_local[1];
	segger_hitavg_local_pz[seg_index] = trk_avg_dir_local[2];
      }
      
      sLumiBlock = eventInfo->lumiBlock(); 
      
      stree->Fill();
      
    }//end segment loop    
  }//end muon loop

  
  // Primary vertex information
  const xAOD::VertexContainer* vxContainer = 0;
  const xAOD::Vertex* primaryVertex = 0;
  CHECK( evtStore()->retrieve( vxContainer, "PrimaryVertices") );

  if (vxContainer->size()>0) {   
    // simple loop through and get the primary vertex
    xAOD::VertexContainer::const_iterator vxIter    = vxContainer->begin();
    xAOD::VertexContainer::const_iterator vxIterEnd = vxContainer->end();
    for ( size_t ivtx = 0; vxIter != vxIterEnd; ++vxIter, ++ivtx ){
      // the first and only primary vertex candidate is picked
      if ( (*vxIter)->vertexType() ==  xAOD::VxType::PriVtx){
	primaryVertex = (*vxIter);
	break;
      }
    }
  }

  if (primaryVertex){
    vx_x = primaryVertex->x();
    vx_y = primaryVertex->y();
    vx_z = primaryVertex->z();
    vx_chiSquared = primaryVertex->chiSquared();
    vx_nDOF = primaryVertex->numberDoF();
  }

  //-----------------------------------------------------------------------------
  // SITREE
  // Store segment information (similar to above) directly via the 
  // muon segment container -> NOT GOING FROM MUON TO SEGMENT  
  //-----------------------------------------------------------------------------

  // ------------- Loop through muon segments
  ATH_MSG_DEBUG( "---------------------- Retrieve muon segments" );
  const xAOD::MuonSegmentContainer* muonsegs = 0;
  CHECK( evtStore()->retrieve( muonsegs, "MuonSegments" ) );
  hist("hist_nSegments")->Fill( muonsegs->size() );

  ATH_MSG_DEBUG( "---------------------- Starting loop over segments" );
  for (size_t segitr = 0; segitr < muonsegs->size(); segitr++){
    //for( const auto* muonseg : *muonsegs ) {
    const xAOD::MuonSegment *muonseg = muonsegs->at(segitr);
    int thisseg = segitr;
    ATH_MSG_DEBUG( "---------------------- To remove all barrel chambers" );
    if (muonseg->chamberIndex() <= 5) //Below this value have barrel chambers (other than BEE)
      continue;
    ATH_MSG_DEBUG( "---------------------- Segment iterator is: " << thisseg );

    const ElementLink<DataVector<Trk::Segment> > muonseg_link = muonseg->muonSegment();
    if (!muonseg_link.isValid()){
      ATH_MSG_DEBUG( "ElementLink from xAOD::MuonSegment to Trk::Segement not valid" );
      continue;
    }
    
    const Trk::Segment *seg = segs->at(muonseg_link.persIndex());
    Muon::MuonSegment *mseg = (Muon::MuonSegment*) seg; //Muon::MuonSegment(seg1);
    const Trk::LocalDirection& locdir_mseg = mseg->localDirection();
    const Trk::PlaneSurface& psurf_mseg = mseg->associatedSurface();

    // Can discard the segments that don't meet hit requirements or quality requirements if want to slim output info...
    bool hitreq = (CUT_hitsOnSegment (muonseg)); //check that have valid number of hits for this chamber type
    bool fitqual = (CUT_fitqualityOfSegment (muonseg)); //check that the fit quality of the segment is adequate
    // if (!hitreq || !fitqual)
    //   continue;


    //Checking Segments:
    int n_Segs = numSegHits( *mseg );
    int n_TrackHits =  0; //numTrackHits( muontr );
    int n_Seg_Track_OvLp = 0; //segtrackHitsOverlap(seg, muontr);
    
    Identifier chid = getChId( (*mseg) );
    Amg::Transform3D gToStationCheck = (*mseg).associatedSurface().transform().inverse();
    Amg::Transform3D gToStation = getGlobalToStation( chid );
    
    // create the local position and direction vector
    Amg::Vector3D  segPosG((*mseg).globalPosition());
    Amg::Vector3D segDirG((*mseg).globalDirection());
    
    // calculate local position and direction of segment
    Amg::Vector3D segPosL  = gToStation*segPosG;
    Amg::Vector3D segDirL = gToStation.linear()*segDirG;
    Amg::Vector3D segDirLCheck = gToStationCheck.linear()*segDirG;
    
    bool anysharedhits = false;
    
    const std::vector<const Trk::MeasurementBase*> segI_vMB = seg->containedMeasurements();
    std::vector<bool> v_match;
    segI_vMB_x.clear();
    segI_vMB_y.clear();
    segI_vMB_z.clear();
    std::vector<int> segI_hit_count;
    
    for (size_t i = 0; i < segI_vMB.size(); i++){
      int currentHits = segI_vMB.size();
      const Trk::MeasurementBase* segMB = segI_vMB[i];
      const Amg::Vector3D& segMB_GP = segMB->globalPosition();
      segI_vMB_x.push_back(segMB_GP.x());
      segI_vMB_y.push_back(segMB_GP.y());
      segI_vMB_z.push_back(segMB_GP.z());
      v_match.push_back(false);
    }
    
    
    //int BaseAnalysis::numSegSegHitShare( Muon::MuonSegment& seg, Muon::MuonSegment &segger )
    const xAOD::MuonSegmentContainer* muonsegs_t = 0; // a const here as we will not modify this container
    CHECK( evtStore()->retrieve( muonsegs_t, "MuonSegments" ) );
    int zcount = 0;
    for (size_t segitr_t = 0; segitr_t < muonsegs_t->size(); segitr_t++){
      const xAOD::MuonSegment *muonsegI_t = muonsegs_t->at(segitr_t);
      const ElementLink<DataVector<Trk::Segment> > muonsegI_t_link = muonsegI_t->muonSegment();
      if (!muonsegI_t_link.isValid())
	ATH_MSG_DEBUG( "ElementLink from xAOD::MuonSegment to Trk::Segement not valid" );
      const Trk::Segment *segI_t = segs->at(muonsegI_t_link.persIndex());
      Muon::MuonSegment *msegr = (Muon::MuonSegment*) segI_t; //Muon::MuonSegment(seg1);
      
      zcount = numSegSegHitShare(*mseg, *msegr);
      segI_hit_count.push_back(zcount);
    }
    
    for (size_t i = 0; i < v_match.size(); i++){
      int valid = 0;
      int currentHits = 0; //segI_vMB.size();
      const Trk::MeasurementBase* segMB = segI_vMB[i];
      const Amg::Vector3D& segMB_GP = segMB->globalPosition();
      
      ATH_MSG_DEBUG( "---------------------- Retrieve muon segments" );
      const xAOD::MuonSegmentContainer* muonsegs_t = 0; // a const here as we will not modify this container
      CHECK( evtStore()->retrieve( muonsegs_t, "MuonSegments" ) );
      ATH_MSG_DEBUG( "---------------------- Starting loop over segments" );
      for (size_t segitr_t = 0; segitr_t < muonsegs_t->size(); segitr_t++){
	//for( const auto* muonseg : *muonsegs ) {
	const xAOD::MuonSegment *muonsegI_t = muonsegs_t->at(segitr_t);
	const ElementLink<DataVector<Trk::Segment> > muonsegI_t_link = muonsegI_t->muonSegment();
	if (!muonsegI_t_link.isValid())
	  ATH_MSG_DEBUG( "ElementLink from xAOD::MuonSegment to Trk::Segement not valid" );
	const Trk::Segment *segI_t = segs->at(muonsegI_t_link.persIndex());
	const std::vector<const Trk::MeasurementBase*> segI_t_vMB = segI_t->containedMeasurements();
	//Don't care about matches between same segment

	if (segI_vMB == segI_t_vMB)
	  continue;
	
	for (size_t j = 0; j < segI_t_vMB.size(); j++){
	  const Trk::MeasurementBase* segMB_t = segI_t_vMB[j];
	  const Amg::Vector3D& segMB_t_GP = segMB_t->globalPosition();
	  
	  if (segMB->globalPosition() == segMB_t->globalPosition()){
	    valid++; //We're matching to full Seg container (so will find a match with self)
	  }
	}//End secondary measurement loop
      }//End secondary segment loop
      if (valid > currentHits) //Need value other than self match
	v_match[i] = true;

    }//End first measurment loop
    
    for (int b = 0; b < v_match.size(); b++){
      if (v_match[b] == true)
	anysharedhits = true;
    }


    segI_nhits = n_Segs;
    segI_ntrackhits = n_TrackHits;
    segI_track_hitshare = n_Seg_Track_OvLp;
    segI_hitshare = segI_hit_count; //anysharedhits;
    sIRunNumber = eventInfo->runNumber();
    sIEventNumber = eventInfo->eventNumber();
    segI_muon_n = muon_n;
    segI_author = seg->author();
    segI_eta_index = muonseg->etaIndex();
    segI_tech = muonseg->technology();
    segI_cham_index = muonseg->chamberIndex();
    segI_name = hitChamberType(muonseg);
    segI_name_short = segI_name.std::string::substr(0,2);;
    segI_haveTrkSeg = 0.;    
    //      segI_ientifier = chid;
    segI_x = muonseg->x();
    segI_y = muonseg->y();
    segI_z = muonseg->z();
    segI_px = muonseg->px();
    segI_py = muonseg->py();
    segI_pz = muonseg->pz();
    segI_t0 = muonseg->t0();
    segI_t0_error = muonseg->t0error();
    segI_chisquare = muonseg->chiSquared();
    segI_nDOF = muonseg->numberDoF();
    segI_nPrecisionHits = muonseg->nPrecisionHits();
    segI_nTrigEtaLayers = muonseg->nTrigEtaLayers();
    segI_nTrigPhiLayers = muonseg->nPhiLayers();
    segI_global_x = segPosG.x();
    segI_global_y = segPosG.y();
    segI_global_z = segPosG.z();
    segI_global_px = segDirG.x();
    segI_global_py = segDirG.y();
    segI_global_pz = segDirG.z();
    segI_local_x = segPosL.x();
    segI_local_y = segPosL.y();
    segI_local_z = segPosL.z();
    segI_local_px = segDirL.x();
    segI_local_py = segDirL.y();
    segI_local_pz = segDirL.z();
    //segI_cham_x = ideal_loc.x();
    //segI_cham_y = ideal_loc.y();
    //segI_cham_z = ideal_loc.z();
    segI_hitreq = (CUT_hitsOnSegment (muonseg)); //check that have valid number of hits for this chamber type
    segI_fitqual = (CUT_fitqualityOfSegment (muonseg)); //check that the fit quality of the segment is adequate
    


    int muonsegEtaIndex = muonseg->etaIndex(); //As far as I can tell the etaIndex refers to the chamber value
    int muonsegSector = muonseg->sector();
    std::string name = hitChamberType(muonseg);
    std::string nshort = name.std::string::substr(0,2);
    ATH_MSG_DEBUG( "---------------------- Load vectors for A side segments" );
    //keeping count of number of segments in sectors (both A and C side) and filling vectors
    if (muonsegEtaIndex > 0){
      countSegInSecXAside[muonsegSector - 1]++;
      if (nshort == "ei") EI_Aside_seg[muonsegSector - 1].push_back(std::make_pair(name, thisseg));
      if (nshort == "ee") EE_Aside_seg[muonsegSector - 1].push_back(std::make_pair(name, thisseg));
      if (nshort == "em") EM_Aside_seg[muonsegSector - 1].push_back(std::make_pair(name, thisseg));
      if (nshort == "eo") EO_Aside_seg[muonsegSector - 1].push_back(std::make_pair(name, thisseg));
      if (nshort == "cs") CSC_Aside_seg[muonsegSector - 1].push_back(std::make_pair(name, thisseg));
      if (nshort == "be") BEE_Aside_seg[muonsegSector - 1].push_back(std::make_pair(name, thisseg));
    }
    ATH_MSG_DEBUG( "---------------------- Load vectors for C side segments" );
    if (muonsegEtaIndex < 0){
      countSegInSecXCside[muonsegSector - 1]++;
      if (nshort == "ei") EI_Cside_seg[muonsegSector - 1].push_back(std::make_pair(name, thisseg));
      if (nshort == "ee") EE_Cside_seg[muonsegSector - 1].push_back(std::make_pair(name, thisseg));
      if (nshort == "em") EM_Cside_seg[muonsegSector - 1].push_back(std::make_pair(name, thisseg));
      if (nshort == "eo") EO_Cside_seg[muonsegSector - 1].push_back(std::make_pair(name, thisseg));
      if (nshort == "cs") CSC_Cside_seg[muonsegSector - 1].push_back(std::make_pair(name, thisseg));
      if (nshort == "be") BEE_Cside_seg[muonsegSector - 1].push_back(std::make_pair(name, thisseg));
    }
    ATH_MSG_DEBUG( "---------------------- Final line of segment loop" );
    sILumiBlock = eventInfo->lumiBlock();
    sItree->Fill();
  }//close muon nsegments loop






  //Discard those muons that have < 3 or > 5 segments in the same sector
  bool validSegInSecXAside[16] = {false};
  bool validSegInSecXCside[16] = {false};
  ATH_MSG_DEBUG( "---------------------- Count for number os segments per sector on A and C side" );
  for (int i = 0; i < 16; i++){
    validSegInSecXAside[i] = CUT_segsPerSector(countSegInSecXAside[i]);
    validSegInSecXCside[i] = CUT_segsPerSector(countSegInSecXCside[i]);
  }

  ATH_MSG_DEBUG( "---------------------- Check whether the a combination is valid" );
  //comb1 for A and C side (this is EI-EM-EO)
  bool comb1A[16] = {false};
  bool comb1C[16] = {false};
  //comb2 for A and C side (this is CSC-EM-EO)
  bool comb2A[16] = {false};
  bool comb2C[16] = {false};
  //comb3 for A and C side (this is EI-EE-EM)
  bool comb3A[16] = {false};
  bool comb3C[16] = {false};
  //comb4 for A and C side (this is EI-BEE-EM)
  bool comb4A[16] = {false};
  bool comb4C[16] = {false};

  //Now sector by sector look through the combinations and check whether they are valid
  for (int i = 0; i < 16; i++){
    if (validSegInSecXAside[i] && EM_Aside_seg[i].size() > 0){ //EM in all combinations
      if (EI_Aside_seg[i].size() > 0 && EO_Aside_seg[i].size() > 0) comb1A[i] = true;
      if (CSC_Aside_seg[i].size() > 0 && EO_Aside_seg[i].size() > 0) comb2A[i] = true;
      if (EI_Aside_seg[i].size() > 0 && EE_Aside_seg[i].size() > 0) comb3A[i] = true;
      if (EI_Aside_seg[i].size() > 0 && BEE_Aside_seg[i].size() > 0) comb4A[i] = true;
    }
    if (validSegInSecXCside[i] && EM_Cside_seg[i].size() > 0){ //EM in all combinations
      if (EI_Cside_seg[i].size() > 0 && EO_Cside_seg[i].size() > 0) comb1C[i] = true;
      if (CSC_Cside_seg[i].size() > 0 && EO_Cside_seg[i].size() > 0) comb2C[i] = true;
      if (EI_Cside_seg[i].size() > 0 && EE_Cside_seg[i].size() > 0) comb3C[i] = true;
      if (EI_Cside_seg[i].size() > 0 && BEE_Cside_seg[i].size() > 0) comb4C[i] = true;
    }
  }
  



  //Need to fill tree once per combination.
  ///All combinations into an array:
  bool combs[8][16];
  for (int i = 0; i < 16; i++){
    combs[0][i] = comb1A[i];  combs[1][i] = comb1C[i];
    combs[2][i] = comb2A[i];  combs[3][i] = comb2C[i];
    combs[4][i] = comb3A[i];  combs[5][i] = comb3C[i];
    combs[6][i] = comb4A[i];  combs[7][i] = comb4C[i];
  }

  //    bool combs[i] = comb1A;//= { comb1A[], comb1C[], comb2A[], comb2C[], comb3A[], comb3C[], comb4A[], comb4C[]};
  std::vector<std::pair<std::string,int> > combtype[8][3][16];
  
for (int i=0; i<16; i++){
    combtype[0][0][i] = EI_Aside_seg[i];
    combtype[0][1][i] = EM_Aside_seg[i];
    combtype[0][2][i] = EO_Aside_seg[i];
    combtype[1][0][i] = EI_Cside_seg[i];
    combtype[1][1][i] = EM_Cside_seg[i];
    combtype[1][2][i] = EO_Cside_seg[i];
    combtype[2][0][i] = CSC_Aside_seg[i];
    combtype[2][1][i] = EM_Aside_seg[i];
    combtype[2][2][i] = EO_Aside_seg[i];
    combtype[3][0][i] = CSC_Cside_seg[i];
    combtype[3][1][i] = EM_Cside_seg[i];
    combtype[3][2][i] = EO_Cside_seg[i];
    combtype[4][0][i] = EI_Aside_seg[i];
    combtype[4][1][i] = EE_Aside_seg[i];
    combtype[4][2][i] = EM_Aside_seg[i];
    combtype[5][0][i] = EI_Cside_seg[i];
    combtype[5][1][i] = EE_Cside_seg[i];
    combtype[5][2][i] = EM_Cside_seg[i];
    combtype[6][0][i] = EI_Aside_seg[i];
    combtype[6][1][i] = BEE_Aside_seg[i];
    combtype[6][2][i] = EM_Aside_seg[i];
    combtype[7][0][i] = EI_Cside_seg[i];
    combtype[7][1][i] = BEE_Cside_seg[i];
    combtype[7][2][i] = EM_Cside_seg[i];
  };

	

  ATH_MSG_DEBUG( "---------------------- Work through each combination per sector" );
  for (int k = 0; k < 8; k++){
    cRunNumber = eventInfo->runNumber();
    cEventNumber = eventInfo->eventNumber() ;
    comb_muon_n = -1;
    if (k == 0 || k == 1) comb_type = "seg";
    if (k == 2 || k == 3) comb_type = "scg";
    if (k == 4 || k == 5) comb_type = "see";
    if (k == 6 || k == 7) comb_type = "seb";

    comb_side = k; 

    for (int i = 0; i < 16; i++){
      bool fill;
      comb_sector = i+1;
      

      fill = false;
      if (combs[k][i]){
	for (int a = 0; a < combtype[k][0][i].size(); a++){
	  //First segment
	  ATH_MSG_DEBUG( "---------------------- Segment 1");
	  int ind1 = combtype[k][0][i][a].second;
	  const xAOD::MuonSegment* muonseg1 = muonsegs->at(ind1);
	  const ElementLink<DataVector<Trk::Segment> > muonseg1_link = muonseg1->muonSegment();
	  if (!muonseg1_link.isValid()){
	    ATH_MSG_DEBUG( "ElementLink from xAOD::MuonSegment to Trk::Segement not valid" );
      continue;
    }
    ///seg1_index = -1;
	  const Trk::Segment *seg1 = segs->at(muonseg1_link.persIndex());
	  //seg1_index = muonseg1_link.persIndex();
	  Muon::MuonSegment *mseg1 = (Muon::MuonSegment*) seg1; //Muon::MuonSegment(seg1);
	  seg1_index = ind1;
    
	  const Trk::LocalDirection& locdir_mseg1 = mseg1->localDirection();
	  const Trk::PlaneSurface& psurf_mseg1 = mseg1->associatedSurface();
	  
	  const Amg::Vector3D& ideal_loc1 = psurf_mseg1.globalReferencePoint();
	  const Trk::LocalParameters& tryparm1 = mseg1->localParameters();

	  Identifier chid1 = getChId( (*mseg1) );
	  Amg::Transform3D gToStationCheck = (*mseg1).associatedSurface().transform().inverse();
	  Amg::Transform3D gToStation = getGlobalToStation( chid1 );
	
	  // create the local position and direction vector
	  Amg::Vector3D  segPosG((*mseg1).globalPosition());
	  Amg::Vector3D segDirG((*mseg1).globalDirection());
	
	  // calculate local position and direction of segment
	  Amg::Vector3D segPosL  = gToStation*segPosG;
	  Amg::Vector3D segDirL = gToStation.linear()*segDirG;
	  Amg::Vector3D segDirLCheck = gToStationCheck.linear()*segDirG;
      

	  seg1_author = seg1->author();
	  seg1_eta_index = muonseg1->etaIndex();
	  seg1_tech = muonseg1->technology();
	  seg1_cham_index = muonseg1->chamberIndex();
	  seg1_name = hitChamberType(muonseg1);
	  seg1_name_short = seg1_name.std::string::substr(0,2);;

	  seg1_x = muonseg1->x();
	  seg1_y = muonseg1->y();
	  seg1_z = muonseg1->z();
	  seg1_px = muonseg1->px();
	  seg1_py = muonseg1->py();
	  seg1_pz = muonseg1->pz();
	  seg1_t0 = muonseg1->t0();
	  seg1_t0_error = muonseg1->t0error();
	  seg1_chisquare = muonseg1->chiSquared();
	  seg1_nDOF = muonseg1->numberDoF();
	  seg1_nPrecisionHits = muonseg1->nPrecisionHits();
	  seg1_nTrigEtaLayers = muonseg1->nTrigEtaLayers();
	  seg1_nTrigPhiLayers = muonseg1->nPhiLayers();
	  seg1_global_x = segPosG.x();
	  seg1_global_y = segPosG.y();
	  seg1_global_z = segPosG.z();
	  seg1_global_px = segDirG.x();
	  seg1_global_py = segDirG.y();
	  seg1_global_pz = segDirG.z();
	  seg1_local_x = segPosL.x();
	  seg1_local_y = segPosL.y();
	  seg1_local_z = segPosL.z();
	  seg1_local_px = segDirL.x();
	  seg1_local_py = segDirL.y();
	  seg1_local_pz = segDirL.z();
	  seg1_cham_x = ideal_loc1.x();
	  seg1_cham_y = ideal_loc1.y();
	  seg1_cham_z = ideal_loc1.z();
	  seg1_hitreq = (CUT_hitsOnSegment (muonseg1)); //check that have valid number of hits for this chamber type
	  seg1_fitqual = (CUT_fitqualityOfSegment (muonseg1)); //check that the fit quality of the segment is adequate


	  if (seg1_index != -1){

	    segger1_author = segger_author[seg1_index];
	    segger1_muon_author = segger_muon_author[seg1_index];
	    segger1_hitsOnTrack = segger_hitsOnTrack[seg1_index];
  
	    segger1_muon_pt = segger_muon_pt[seg1_index];
	    segger1_muon_eta = segger_muon_eta[seg1_index];
	    segger1_muon_phi = segger_muon_phi[seg1_index];

	    segger1_nhits = segger_nhits[seg1_index];
	    segger1_ntrackhits = segger_ntrackhits[seg1_index];
	    segger1_track_hitshare = segger_track_hitshare[seg1_index];
	    segger1_hitshare = segger_hitshare[seg1_index];


	    segger1_trk_match_id_count = segger_trk_match_id_count[seg1_index];
	    segger1_hitavg_global_x = segger_hitavg_global_x[seg1_index];
	    segger1_hitavg_global_y = segger_hitavg_global_y[seg1_index];
	    segger1_hitavg_global_z = segger_hitavg_global_z[seg1_index];
	    segger1_hitavg_local_x = segger_hitavg_local_x[seg1_index];
	    segger1_hitavg_local_y = segger_hitavg_local_y[seg1_index];
	    segger1_hitavg_local_z = segger_hitavg_local_z[seg1_index];
	    segger1_hitavg_global_px = segger_hitavg_global_px[seg1_index];
	    segger1_hitavg_global_py = segger_hitavg_global_py[seg1_index];
	    segger1_hitavg_global_pz = segger_hitavg_global_pz[seg1_index];
	    segger1_hitavg_local_px = segger_hitavg_local_px[seg1_index];
	    segger1_hitavg_local_py = segger_hitavg_local_py[seg1_index];
	    segger1_hitavg_local_pz = segger_hitavg_local_pz[seg1_index];


	  } else {
	    
	    std::vector<int> segger_temp_hitshare;

	    segger1_author = -99999;
	    segger1_muon_author =  -99999;
	    segger1_hitsOnTrack =  -99999.;
	    segger1_muon_pt =  -99999.;
	    segger1_muon_eta =  -99999.;
	    segger1_muon_phi =  -99999.;

	    segger1_nhits =  -99999;
	    segger1_ntrackhits =  -99999;
	    segger1_track_hitshare =  -99999;
	    segger1_hitshare = segger_temp_hitshare;

	    segger1_trk_match_id_count = -99999;
	    segger1_hitavg_global_x =  -99999;
	    segger1_hitavg_global_y =  -99999;
	    segger1_hitavg_global_z =  -99999;
	    segger1_hitavg_local_x =  -99999;
	    segger1_hitavg_local_y =  -99999;
	    segger1_hitavg_local_z =  -99999;
	    segger1_hitavg_global_px =  -99999;
	    segger1_hitavg_global_py =  -99999;
	    segger1_hitavg_global_pz =  -99999;
	    segger1_hitavg_local_px =  -99999;
	    segger1_hitavg_local_py =  -99999;
	    segger1_hitavg_local_pz =  -99999;

	  }


	 
	  TVector3 first;
	  first.SetXYZ(segPosG.x(), segPosG.y(), segPosG.z());
	  double tp_phi = first.Phi(); 
	  double tp_eta = first.Eta(); 
	  double dR = 100.;

	  int track_index = trkMatchIndex(tp_phi, tp_eta, dR);
	  if (track_index == -1 || dR == 100.){
	    ATH_MSG_INFO("------------- No muon trkTrack ");
	    //  continue;
	  }
	
	  //  const Trk::Track *muonst = tracks->at(track_index);
	
	  for (int b = 0; b < combtype[k][1][i].size(); b++){
	    //Second segment
	    ATH_MSG_DEBUG( "---------------------- Segment 2");	  
	    const xAOD::MuonSegment* muonseg2 = muonsegs->at(combtype[k][1][i][b].second);
	    const ElementLink<DataVector<Trk::Segment> > muonseg2_link = muonseg2->muonSegment();
	    if (!muonseg2_link.isValid()){
        ATH_MSG_DEBUG( "ElementLink from xAOD::MuonSegment to Trk::Segement not valid" );
        continue;
      }

	    seg2_index = -1;
	    const Trk::Segment *seg2 = segs->at(muonseg2_link.persIndex());
	    // seg2_index = muonseg2_link.persIndex();
	    seg2_index = combtype[k][1][i][b].second;
	    Muon::MuonSegment *mseg2 = (Muon::MuonSegment*) seg2; //Muon::MuonSegment(seg1);
	    const Trk::LocalDirection& locdir_mseg2 = mseg2->localDirection();
	    const Trk::PlaneSurface& psurf_mseg2 = mseg2->associatedSurface();
	    //First segment

	    const Amg::Vector3D& ideal_loc2 = psurf_mseg2.globalReferencePoint();
	    const Trk::LocalParameters& tryparm2 = mseg2->localParameters();


	    Identifier chid2 = getChId( (*mseg2) );
	    Amg::Transform3D gToStationCheck2 = (*mseg2).associatedSurface().transform().inverse();
	    Amg::Transform3D gToStation2 = getGlobalToStation( chid2 );
	
	    // create the local position and direction vector
	    Amg::Vector3D  seg2PosG((*mseg2).globalPosition());
	    Amg::Vector3D seg2DirG((*mseg2).globalDirection());
	  
	    // calculate local position and direction of segment
	    Amg::Vector3D seg2PosL  = gToStation2*seg2PosG;
	    Amg::Vector3D seg2DirL = gToStation2.linear()*seg2DirG;
	    Amg::Vector3D seg2DirLCheck = gToStationCheck2.linear()*seg2DirG;
	  

	    seg2_author = seg2->author();
	    seg2_eta_index = muonseg2->etaIndex();
	    seg2_tech = muonseg2->technology();
	    seg2_cham_index = muonseg2->chamberIndex();
	    seg2_name = hitChamberType(muonseg2);
	    seg2_name_short = seg2_name.std::string::substr(0,2);;
	    //seg2_haveTrkSeg = muonTrmatch;
	    //      seg_identifier = chid;
	    seg2_x = muonseg2->x();
	    seg2_y = muonseg2->y();
	    seg2_z = muonseg2->z();
	    seg2_px = muonseg2->px();
	    seg2_py = muonseg2->py();
	    seg2_pz = muonseg2->pz();
	    seg2_t0 = muonseg2->t0();
	    seg2_t0_error = muonseg2->t0error();
	    seg2_chisquare = muonseg2->chiSquared();
	    seg2_nDOF = muonseg2->numberDoF();
	    seg2_nPrecisionHits = muonseg2->nPrecisionHits();
	    seg2_nTrigEtaLayers = muonseg2->nTrigEtaLayers();
	    seg2_nTrigPhiLayers = muonseg2->nPhiLayers();
	    seg2_global_x = seg2PosG.x();
	    seg2_global_y = seg2PosG.y();
	    seg2_global_z = seg2PosG.z();
	    seg2_global_px = seg2DirG.x();
	    seg2_global_py = seg2DirG.y();
	    seg2_global_pz = seg2DirG.z();
	    seg2_local_x = seg2PosL.x();
	    seg2_local_y = seg2PosL.y();
	    seg2_local_z = seg2PosL.z();
	    seg2_local_px = seg2DirL.x();
	    seg2_local_py = seg2DirL.y();
	    seg2_local_pz = seg2DirL.z();
	    seg2_cham_x = ideal_loc2.x();
	    seg2_cham_y = ideal_loc2.y();
	    seg2_cham_z = ideal_loc2.z();
	    seg2_hitreq = (CUT_hitsOnSegment (muonseg2)); //check that have valid number of hits for this chamber type
	    seg2_fitqual = (CUT_fitqualityOfSegment (muonseg2)); //check that the fit quality of the segment is adequate

	    if (seg2_index != -1){

	      segger2_trk_match_id_count = segger_trk_match_id_count[seg2_index];
	      segger2_hitavg_global_x = segger_hitavg_global_x[seg2_index];
	      segger2_hitavg_global_y = segger_hitavg_global_y[seg2_index];
	      segger2_hitavg_global_z = segger_hitavg_global_z[seg2_index];
	      segger2_hitavg_local_x = segger_hitavg_local_x[seg2_index];
	      segger2_hitavg_local_y = segger_hitavg_local_y[seg2_index];
	      segger2_hitavg_local_z = segger_hitavg_local_z[seg2_index];
	      segger2_hitavg_global_px = segger_hitavg_global_px[seg2_index];
	      segger2_hitavg_global_py = segger_hitavg_global_py[seg2_index];
	      segger2_hitavg_global_pz = segger_hitavg_global_pz[seg2_index];
	      segger2_hitavg_local_px = segger_hitavg_local_px[seg2_index];
	      segger2_hitavg_local_py = segger_hitavg_local_py[seg2_index];
	      segger2_hitavg_local_pz = segger_hitavg_local_pz[seg2_index];

	      segger2_author = segger_author[seg2_index];
	      segger2_muon_author = segger_muon_author[seg2_index];
	      segger2_hitsOnTrack = segger_hitsOnTrack[seg2_index];
	    
	      segger2_muon_pt = segger_muon_pt[seg2_index];
	      segger2_muon_eta = segger_muon_eta[seg2_index];
	      segger2_muon_phi = segger_muon_phi[seg2_index];

	      segger2_nhits = segger_nhits[seg2_index];
	      segger2_ntrackhits = segger_ntrackhits[seg2_index];
	      segger2_track_hitshare = segger_track_hitshare[seg2_index];
	      segger2_hitshare = segger_hitshare[seg2_index];
	    } else {

	      std::vector<int> segger2_temp_hitshare;

	      segger2_author = -99999;
	      segger2_muon_author =  -99999;
	      segger2_hitsOnTrack =  -99999.;
	      segger2_muon_pt =  -99999.;
	      segger2_muon_eta =-99999.;
	      segger2_muon_phi =-99999.;

	      segger2_nhits =  -99999;
	      segger2_ntrackhits =  -99999;
	      segger2_track_hitshare =  -99999;
	      segger2_hitshare = segger2_temp_hitshare;

	      segger2_trk_match_id_count = -99999;
	      segger2_hitavg_global_x =  -99999;
	      segger2_hitavg_global_y =  -99999;
	      segger2_hitavg_global_z =  -99999;
	      segger2_hitavg_local_x =  -99999;
	      segger2_hitavg_local_y =  -99999;
	      segger2_hitavg_local_z =  -99999;
	      segger2_hitavg_global_px =  -99999;
	      segger2_hitavg_global_py =  -99999;
	      segger2_hitavg_global_pz =  -99999;
	      segger2_hitavg_local_px =  -99999;
	      segger2_hitavg_local_py =  -99999;
	      segger2_hitavg_local_pz =  -99999;

	    }

	    
	  
	    for (int c = 0; c < combtype[k][2][i].size(); c++){
	      //Third segment
	      ATH_MSG_DEBUG( "---------------------- Segment 3");
  
	      const xAOD::MuonSegment* muonseg3 = muonsegs->at(combtype[k][2][i][c].second);

	      const ElementLink<DataVector<Trk::Segment> > muonseg3_link = muonseg3->muonSegment();
	      if (!muonseg3_link.isValid()){
          ATH_MSG_DEBUG( "ElementLink from xAOD::MuonSegment to Trk::Segement not valid" );
          continue;
        }


	      seg3_index = -1;
	      const Trk::Segment *seg3 = segs->at(muonseg3_link.persIndex());
	      //seg3_index = muonseg3_link.persIndex();
	      seg3_index = combtype[k][2][i][c].second;
	      Muon::MuonSegment *mseg3 = (Muon::MuonSegment*) seg3; //Muon::MuonSegment(seg1);
	      const Trk::LocalDirection& locdir_mseg3 = mseg3->localDirection();
	      const Trk::PlaneSurface& psurf_mseg3 = mseg3->associatedSurface();
	      //First segment

	      const Amg::Vector3D& ideal_loc3 = psurf_mseg3.globalReferencePoint();
	      const Trk::LocalParameters& tryparm3 = mseg3->localParameters();
	      
	      Identifier chid3 = getChId( (*mseg3) );
	      Amg::Transform3D gToStationCheck3 = (*mseg3).associatedSurface().transform().inverse();
	      Amg::Transform3D gToStation3 = getGlobalToStation( chid3 );

	      // create the local position and direction vector
	      Amg::Vector3D  seg3PosG((*mseg3).globalPosition());
	      Amg::Vector3D seg3DirG((*mseg3).globalDirection());
	      
	      // calculate local position and direction of segment
	      Amg::Vector3D seg3PosL  = gToStation3*seg3PosG;
	      Amg::Vector3D seg3DirL = gToStation3.linear()*seg3DirG;
	      Amg::Vector3D seg3DirLCheck = gToStationCheck3.linear()*seg3DirG;
	      

	      seg3_author = seg3->author();
	      seg3_eta_index = muonseg3->etaIndex();
	      seg3_tech = muonseg3->technology();
	      seg3_cham_index = muonseg3->chamberIndex();
	      seg3_name = hitChamberType(muonseg3);
	      seg3_name_short = seg3_name.std::string::substr(0,2);;
	      //seg3_haveTrkSeg = muonTrmatch;
	      //      seg_identifier = chid;
	      seg3_x = muonseg3->x();
	      seg3_y = muonseg3->y();
	      seg3_z = muonseg3->z();
	      seg3_px = muonseg3->px();
	      seg3_py = muonseg3->py();
	      seg3_pz = muonseg3->pz();
	      seg3_t0 = muonseg3->t0();
	      seg3_t0_error = muonseg3->t0error();
	      seg3_chisquare = muonseg3->chiSquared();
	      seg3_nDOF = muonseg3->numberDoF();
	      seg3_nPrecisionHits = muonseg3->nPrecisionHits();
	      seg3_nTrigEtaLayers = muonseg3->nTrigEtaLayers();
	      seg3_nTrigPhiLayers = muonseg3->nPhiLayers();
	      seg3_global_x = seg3PosG.x();
	      seg3_global_y = seg3PosG.y();
	      seg3_global_z = seg3PosG.z();
	      seg3_global_px = seg3DirG.x();
	      seg3_global_py = seg3DirG.y();
	      seg3_global_pz = seg3DirG.z();
	      seg3_local_x = seg3PosL.x();
	      seg3_local_y = seg3PosL.y();
	      seg3_local_z = seg3PosL.z();
	      seg3_local_px = seg3DirL.x();
	      seg3_local_py = seg3DirL.y();
	      seg3_local_pz = seg3DirL.z();
	      seg3_cham_x = ideal_loc3.x();
	      seg3_cham_y = ideal_loc3.y();
	      seg3_cham_z = ideal_loc3.z();
	      seg3_hitreq = (CUT_hitsOnSegment (muonseg3)); //check that have valid number of hits for this chamber type
	      seg3_fitqual = (CUT_fitqualityOfSegment (muonseg3)); //check that the fit quality of the segment is adequate

	      if (seg3_index != -1){

		segger3_trk_match_id_count = segger_trk_match_id_count[seg3_index];
		segger3_hitavg_global_x = segger_hitavg_global_x[seg3_index];
		segger3_hitavg_global_y = segger_hitavg_global_y[seg3_index];
		segger3_hitavg_global_z = segger_hitavg_global_z[seg3_index];
		segger3_hitavg_local_x = segger_hitavg_local_x[seg3_index];
		segger3_hitavg_local_y = segger_hitavg_local_y[seg3_index];
		segger3_hitavg_local_z = segger_hitavg_local_z[seg3_index];
		segger3_hitavg_global_px = segger_hitavg_global_px[seg3_index];
		segger3_hitavg_global_py = segger_hitavg_global_py[seg3_index];
		segger3_hitavg_global_pz = segger_hitavg_global_pz[seg3_index];
		segger3_hitavg_local_px = segger_hitavg_local_px[seg3_index];
		segger3_hitavg_local_py = segger_hitavg_local_py[seg3_index];
		segger3_hitavg_local_pz = segger_hitavg_local_pz[seg3_index];

		segger3_author = segger_author[seg3_index];
		segger3_muon_author = segger_muon_author[seg3_index];
		segger3_hitsOnTrack = segger_hitsOnTrack[seg3_index];
	     
		segger3_muon_pt = segger_muon_pt[seg3_index];
		segger3_muon_eta = segger_muon_eta[seg3_index];
		segger3_muon_phi = segger_muon_phi[seg3_index];

		segger3_nhits = segger_nhits[seg3_index];
		segger3_ntrackhits = segger_ntrackhits[seg3_index];
		segger3_track_hitshare = segger_track_hitshare[seg3_index];
		segger3_hitshare = segger_hitshare[seg3_index];

	      } else {

		std::vector<int> segger3_temp_hitshare;

		segger3_author = -99999;
		segger3_muon_author =  -99999;
		segger3_hitsOnTrack =  -99999.;
		segger3_muon_pt =  -99999.;
		segger3_muon_eta =-99999.;
		segger3_muon_phi =-99999.;

		segger3_nhits =  -99999;
		segger3_ntrackhits =  -99999;
		segger3_track_hitshare =  -99999;
		segger3_hitshare = segger3_temp_hitshare;

		segger3_trk_match_id_count = -99999;
		segger3_hitavg_global_x =  -99999;
		segger3_hitavg_global_y =  -99999;
		segger3_hitavg_global_z =  -99999;
		segger3_hitavg_local_x =  -99999;
		segger3_hitavg_local_y =  -99999;
		segger3_hitavg_local_z =  -99999;
		segger3_hitavg_global_px =  -99999;
		segger3_hitavg_global_py =  -99999;
		segger3_hitavg_global_pz =  -99999;
		segger3_hitavg_local_px =  -99999;
		segger3_hitavg_local_py =  -99999;
		segger3_hitavg_local_pz =  -99999;

	      }

	      const std::vector<const Trk::MeasurementBase*> containedMasurments3 = seg3->containedMeasurements();
	      for (unsigned int x = 0; x < seg3->numberOfMeasurementBases(); x++){
		const Trk::MeasurementBase* mes1 = seg3->measurement(x);
		const Trk::LocalParameters locpar1 = mes1->localParameters();
	      }
	      std::string histname = "h_NOM_"+combtype[k][0][i][a].first+"_"+combtype[k][1][i][b].first+"_"+combtype[k][2][i][c].first;

	      //hist(histname.c_str())->Fill( value );
	      cLumiBlock = eventInfo->lumiBlock(); 


	      tree->Fill();
	      
	    }
	  }
	}
      }
    }
    
  }



  etree->Fill();

  return StatusCode::SUCCESS;
}
