# Alignment Ntuple Anaysis Rel22

new version of the code 
```
git clone https://gitlab.cern.ch/cpazosbo/alignment_toroid_off.git
```

change into that directory
```
cd alignment_rel22
```

then set up the environment and compile. 
```
setupATLAS
source seganalysis_setup.sh
```

should compile. Then:

```
cd ../run
athena ../source/MyAnalysis/share/test_jobOptions.py
```

This *should* produce a file called output.root that you run the *make_aramys.py* script on that to produce the right input for aramys.
```
python make_aramys.py output.root
```






##############################
#### NOTES FROM DEBUGGING ####
##############################

[FIXED ISSUES] (maybe...)

**line 1028, changed "MuonSegments" to "TrackMuonSegments"**

```
// Retrieve the Trk::SegmentCollection:
  const Trk::SegmentCollection* segs = 0; // a const here as we will not modify this container
  CHECK( evtStore()->retrieve( segs, "TrackMuonSegments" ) );
```

**MyAnalysisAlg::segtrackHitsOverlap** - caused a segfault, same reason as getChId (see below)

**MyAnalysisAlg::getChId** - caused segfault, fix -> some muon tracks do not have muon segments, but it tries to use the segment anyway and thats when it crashes. in if-statement checking whether the muon segment is valid, if not valid then continue to next muon; made this change for every muon segment is tries to access.

```
if (!museg_link.isValid()) {
        ATH_MSG_DEBUG( "ElementLink from xAOD::MuonSegment to Trk::Segement not valid" );
        continue;
      }
```




[STATUS]

Code does run without errors, but doesn't fill most of the histograms. I think it's because most of the muon tracks don't have muon segments... this is probably because we are extracting the data from the ESD incorrectly...
